package ar.wsti.tipsmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.Negocio;
import cz.msebera.android.httpclient.Header;

public class PremioActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private View premiosFormView;
    private ListView premiosListView;
    private NegociosAdapter adapter;
    private DrawerLayout drawer;
    private String mActivityTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<Negocio> PremiosArrayList;
    private NegociosAdapter.OnItemSelectedListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premio);

        PremiosArrayList = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            if (getSupportActionBar() !=null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_main);
        mActivityTitle = getTitle().toString();
        setupDrawer();

        pDialog = new ProgressDialog(PremioActivity.this);
        pDialog.setMessage("Cargando contenido...");
        pDialog.setCancelable(false);

        premiosFormView = findViewById(R.id.premiosFormView);
        premiosListView = (ListView) findViewById(R.id.premiosListView);

        listener = new NegociosAdapter.OnItemSelectedListener() {
            @Override
            public void OnItemSelected(Negocio negocio) {
                Intent detalle = new Intent(PremioActivity.this,DetalleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id",negocio.getId());
                bundle.putString("name",negocio.getName());
                bundle.putString("description",negocio.getDescription());
                bundle.putString("photo",negocio.getPhoto());

                detalle.putExtra("detalles",bundle);
                startActivity(detalle);
            }
        };

        try{
            obtenerPremios();
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    private void setupSearch(){
        EditText search = (EditText) findViewById(R.id.buscar);
        if (search!= null) {
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    if (arg0.length() > 0)
                        PremioActivity.this.adapter.getFilter().filter(arg0);
                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    if (arg0.length() == 0) {
                        adapter = new NegociosAdapter(getApplicationContext(),PremiosArrayList,listener,2);
                        premiosListView.setAdapter(adapter);
                        PremioActivity.this.adapter.getFilter().filter(arg0);
                    }
                }
            });
        }
    }

    private void obtenerPremios() throws JSONException {
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        AsyncHttpClientHandler.get("getPremisByClient", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                JSONArray content;
                if (response != null) {
                    try {
                        Log.d("JSONPremio",response.toString());
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        if (success) {

                            content = response.getJSONArray(Constantes.TAG_CONTENT);
                            for (int i = 0;i<content.length();i++) {
                                JSONObject c = content.getJSONObject(i);

                                if (c.getString(Constantes.TAG_STATUS).equals("Active")) {
                                    Negocio negocio = new Negocio();
                                    negocio.setId(c.getString(Constantes.TAG_ID));
                                    negocio.setName(c.getString(Constantes.TAG_NAME));
                                    negocio.setDescription(c.getString(Constantes.TAG_DESCRIPTION));
                                    negocio.setPhoto(c.getString(Constantes.TAG_PHOTO));

                                    PremiosArrayList.add(negocio);
                                }
                            }
                            showProgress(false);
                            adapter = new NegociosAdapter(getApplicationContext(),PremiosArrayList,listener,2);
                            premiosListView.setAdapter(adapter);
                            setupSearch();

                        } else {
                            showProgress(false);
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                } else {
                    showProgress(false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error,JSONObject responseError) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PremioActivity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            obtenerPremios();
                        }catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }

            @Override
            public void onCancel(){
                finish();
            }

        });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.right_in,R.anim.right_out);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_in,R.anim.left_out);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle("Main Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(mDrawerToggle);
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        premiosFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        premiosFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                premiosFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        if (show)
            pDialog.show();
        else
            pDialog.dismiss();

    }
}
