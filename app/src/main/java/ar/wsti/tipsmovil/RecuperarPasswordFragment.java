package ar.wsti.tipsmovil;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


public class RecuperarPasswordFragment extends DialogFragment {


    private OnEnviarCorreoListener mListener;

    public RecuperarPasswordFragment() {
        // Required empty public constructor

    }

    public static RecuperarPasswordFragment newInstance() {
        RecuperarPasswordFragment fragment = new RecuperarPasswordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_recuperar_password, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        TextView emailTxt = (TextView)view.findViewById(R.id.emailRecuperar);
        final String email = emailTxt.getText().toString();
        builder.setTitle(getActivity().getString(R.string.app_name));
        builder.setMessage(getActivity().getString(R.string.ingrese_email));
        builder.setNeutralButton("Cerrar",null);
        builder.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((LoginActivity)getActivity()).onCorreoEnviado(email);
            }
        });
        return builder.create();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEnviarCorreoListener) {
            mListener = (OnEnviarCorreoListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEnviarCorreoListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEnviarCorreoListener {
        void onCorreoEnviado(String email);
    }
}
