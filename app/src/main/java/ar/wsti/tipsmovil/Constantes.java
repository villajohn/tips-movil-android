package ar.wsti.tipsmovil;

public class Constantes {

    //Production Server
    public static final String key = "x1CX-blyl-9NSB";
    //Local Test
//    public static final String key = "92ez-xD5j-h7bk";

    public static final String TAG_KEY = "key";
    public static final String TAG_SUCCESS = "success";
    public static final String TAG_CONTENT = "content";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_LANGUAGE = "language";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_PASSWORD = "password";
    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_USERNAME = "username";
    public static final String TAG_PHONE = "phone";
    public static final String TAG_SEX = "sex";
    public static final String TAG_BIRTH = "birth_date";
    public static final String TAG_CATEGORIES = "categories";
    public static final String TAG_CATEGORY = "category";
    public static final String TAG_PHOTO = "photo";
    public static final String TAG_COUNTRY      = "country";
    public static final String TAG_CITIES_ID    = "cities_id";
    public static final String TAG_COUNTRIES_ID = "countries_id";

    public static final String TAG_DESCRIPTION  = "description";
    public static final String TAG_LATITUDE     = "latitude";
    public static final String TAG_LONGITUDE    = "longitude";
    public static final String TAG_ADDRESS      = "address";
    public static final String TAG_INSTAGRAM    = "instagram";
    public static final String TAG_FACEBOOK     = "facebook";
    public static final String TAG_TWITTER      = "twitter";
    public static final String TAG_CITY         = "city";
    public static final String TAG_UUID         = "uuid";
    public static final String TAG_MAJOR        = "major";
    public static final String TAG_MINOR        = "minor";

    public static final String TAG_CITIES = "cities";
    public static final String TAG_CUSTOMER_ID = "customer_id";
    public static final String TAG_CUSTOMERS_ID = "customers_id";
    public static final String TAG_STATUS = "status";
    public static final String TAG_INTEREST = "interest";

    public static final long gLocationInterval = 30000;
}
