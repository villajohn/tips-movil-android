package ar.wsti.tipsmovil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ar.wsti.tipsmovil.objetos.Usuario;

public class PrefUtils {

    private static final String PREF_NAME = "LogedUser";

    public static void setUsuario(Usuario usuario, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",usuario.getId());
        editor.putString("name",usuario.getNombre());
        editor.putString("email",usuario.getEmail());
        editor.apply();
    }

    public static Usuario getUsuario(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        return new Usuario(sharedPreferences.getString("id","Empty"),sharedPreferences.getString("name","Empty"),sharedPreferences.getString("email","Empty"));
    }

    public static void clearUsuario(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
