package ar.wsti.tipsmovil.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ar.wsti.tipsmovil.DetalleActivity;
import ar.wsti.tipsmovil.R;
import ar.wsti.tipsmovil.activities.MarketingActivity;
import ar.wsti.tipsmovil.objetos.BeaconMessage;

public class DialogActivity extends FragmentActivity {
    private static final String ARG_PARAM2 = "param2";

    /** Instance of the current application. */
    private static DialogActivity instance;

    private SharedPreferences datosBeacon;
    private BeaconMessage beaconMessage;
    private List<BeaconMessage> beaconList = new ArrayList<BeaconMessage>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        Bundle bd = intent.getExtras();
        bd.get("id");
        beaconMessage = new BeaconMessage();
        if (bd != null) {
            String idItem   = (String) bd.get("id");
            String name     = (String) bd.get("name");
            String photo    = (String) bd.get("photo");
            String pubId    = (String) bd.get("pub_id");
            Double distance = (Double) bd.get("distance");
            String video    = (String) bd.get("video");
            int expo        = (int) bd.get("expo_time");
            String texto1   = (String) bd.get("texto1");
            String texto2   = (String) bd.get("texto2");
            Double latitude = (Double) bd.get("latitude");
            Double longitude = (Double) bd.get("longitude");
            Double far      = (Double) bd.get("far");
            String website  = (String) bd.get("website");
            String description = (String) bd.get("description");

            beaconMessage.setmId(idItem);
            beaconMessage.setmName(name);
            beaconMessage.setmPhoto(photo);
            beaconMessage.setmPublicationId(pubId);
            beaconMessage.setmDistance(distance);
            beaconMessage.setmVideoUrl(video);
            beaconMessage.setmExpoTime(expo);
            beaconMessage.setmTexto1(texto1);
            beaconMessage.setmTexto2(texto2);
            beaconMessage.setmLatitude(latitude);
            beaconMessage.setmLongitude(longitude);
            beaconMessage.setmFar(far);
            beaconMessage.setmWebsite(website);
            beaconMessage.setmDescription(description);

        }

        String messageAlert = "";
        switch (Integer.parseInt(beaconMessage.getmPublicationId())) {
            case 1:
                messageAlert = "Un nuevo video que te puede interesar";
                break;
            case 2:
                messageAlert = "Hay una oferta que te puede interesar";
                break;
            case 3:
                messageAlert = "Queremos mostrarte una nueva oferta que te puede itnteresar";
                break;
            case 4:
                messageAlert = beaconMessage.getmTexto1();
                break;
            case 5:
                messageAlert = "Queremos mostrarte una nueva oferta que te puede itnteresar";
                break;
            case 6: messageAlert = "Queremos que escuches una nota de audio que puede interesarte";
                break;
            case 7:
                messageAlert = "Queremos mostrarte un contenido que puede interesarte";
                break;
            default:

        }


        new AlertDialog.Builder(this)
                .setView(getLayoutInflater().inflate(R.layout.fragment_dialog, null))
                .setPositiveButton(this.getResources().getString(R.string.ok).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
                        SharedPreferences.Editor viewEdit = getView.edit();
                        viewEdit.putBoolean("Actual_View", false);
                        viewEdit.apply();

                        Intent detalle = new Intent(DialogActivity.this,MarketingActivity.class);
                        Bundle bundle = new Bundle();

                        bundle.putString("id", beaconMessage.getmId());
                        bundle.putString("name",beaconMessage.getmName());
                        bundle.putString("photo",beaconMessage.getmPhoto());
                        bundle.putString("pub_id", beaconMessage.getmPublicationId());
                        bundle.putDouble("distance", beaconMessage.getmDistance());
                        bundle.putString("video", beaconMessage.getmVideoUrl());
                        bundle.putInt("expo_time", beaconMessage.getmExpoTime());
                        bundle.putString("texto1", beaconMessage.getmTexto1());
                        bundle.putString("texto2", beaconMessage.getmTexto2());
                        bundle.putDouble("latitude", beaconMessage.getmLatitude());
                        bundle.putDouble("longitude", beaconMessage.getmLongitude());
                        bundle.putDouble("far", beaconMessage.getmFar());
                        bundle.putString("website", beaconMessage.getmWebsite());
                        bundle.putString("description", beaconMessage.getmDescription());
                        detalle.putExtra("detalles",bundle);
                        startActivity(detalle);
                    }
                })
                .setNeutralButton(this.getResources().getString(R.string.cerrar).toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
                        SharedPreferences.Editor viewEdit = getView.edit();
                        viewEdit.putBoolean("Actual_View", false);
                        viewEdit.apply();
                        finish();
                    }
                })
                .setIcon(R.drawable.icon_small)
                .setTitle("Atención")
                .setMessage("Deseas ver el aviso: " + messageAlert + " ?")
                .show();

    }

    public DialogActivity() {}

    public int getLayoutId() {
        return R.layout.fragment_dialog;
    }

}
