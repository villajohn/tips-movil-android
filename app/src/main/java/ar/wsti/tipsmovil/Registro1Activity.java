package ar.wsti.tipsmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;

import ar.wsti.tipsmovil.objetos.Categoria;
import ar.wsti.tipsmovil.objetos.Ciudad;
import ar.wsti.tipsmovil.objetos.NavItem;
import ar.wsti.tipsmovil.objetos.Pais;
import ar.wsti.tipsmovil.objetos.Usuario;
import cz.msebera.android.httpclient.Header;

public class Registro1Activity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private View mProgressView;
    private View mRegistroFormView;
    private DrawerLayout drawer;
    private String mActivityTitle;
    private ActionBarDrawerToggle mDrawerToggle;

    private Bundle info_registro;
    TextInputEditText Nombre;
    TextInputEditText Email;
    TextInputEditText Username;
    TextInputEditText Password, PasswordConfirm;
    TextInputEditText Telefono;
    TextInputEditText Nacimiento;
    Button siguiente;
    Spinner PaisSpinner;
    Spinner Cities;
    Spinner Sexo;
    LinkedList<Pais> paisesList = new LinkedList<>();
    String cities_id;
    ArrayList<String> interest;
    String id_usuario;
    String countryUser;
    String cityUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro1);

        info_registro = new Bundle();

        mRegistroFormView = findViewById(R.id.registro1Form);
        mProgressView = findViewById(R.id.progress_bar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_main);
        setupDrawer();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        if(PrefUtils.getUsuario(getApplicationContext()).getId().equals("Empty")){
            mActivityTitle = getString(R.string.registro);
        }else {
            mActivityTitle = getString(R.string.perfil);
            setupNavigationView();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            if (getSupportActionBar() !=null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setTitle(mActivityTitle);
            }
        }

        id_usuario      = "0";
        countryUser     = "";
        cityUser        = "";
        Nombre          = (TextInputEditText) findViewById(R.id.Nombre);
        Email           = (TextInputEditText) findViewById(R.id.Email);
        Username        = (TextInputEditText) findViewById(R.id.Username);
        Password        = (TextInputEditText) findViewById(R.id.Contraseña);
        PasswordConfirm = (TextInputEditText) findViewById(R.id.passWordConfirm);
        Telefono        = (TextInputEditText) findViewById(R.id.Telefono);
        Cities          = (Spinner) findViewById(R.id.Ciudad);
        Nacimiento      = (TextInputEditText) findViewById(R.id.FechaNacimiento);
        PaisSpinner     = (Spinner) findViewById(R.id.Pais);
        Sexo            = (Spinner) findViewById(R.id.Sexo);

        if (Nacimiento != null) {
            Nacimiento.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePickerDialog();
                }
            });
            Nacimiento.setKeyListener(null);
            Nacimiento.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        showDatePickerDialog();

                }
            });
        }

        try {
            obtenerPaises();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] sexos = new String[]{"seleccione sexo","Masculino","Femenino"};
        ArrayAdapter adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_spinner_item,sexos);
        adapter.setDropDownViewResource(R.layout.custom_spinner_item);
        Sexo.setAdapter(adapter);
        Sexo.setSelection(0);

        siguiente = (Button)findViewById(R.id.nextRegistro);

        if (!PrefUtils.getUsuario(getApplicationContext()).getId().equalsIgnoreCase("empty")) {
            try {

                siguiente.setText("Actualizar");
                siguiente.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            registerProfile(info_registro);
                        } catch (JSONException e) {

                        }
                    }
                });
                obtenerDatosUsuario(PrefUtils.getUsuario(getApplicationContext()).getId());
            }catch (JSONException e){
                e.printStackTrace();
            }
        } else {
            siguiente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    realizarRegistro();
                }
            });
        }

    }

    public void showDatePickerDialog() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    // handle the date selected
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // store the values selected into a Calendar instance

        Nacimiento.setText(String.format(Locale.ENGLISH,
                "%d-%d-%d",
                year,monthOfYear + 1,dayOfMonth));

    }

    private void obtenerPaises() throws JSONException {

        AsyncHttpClientHandler.get("getCountries", null, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                llenarListaPaises(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject response) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registro1Activity.this);
                alertDialog.setTitle("Connection error");
                alertDialog.setMessage("There was an error trying to load the content");
                alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            obtenerPaises();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void showCountryUser(String country) {
        int e = 0;
        boolean found = false;
        for (Pais cnt : paisesList) {
            if (found) {
                break;
            }
            if (cnt.getName().equalsIgnoreCase(country)) {
                PaisSpinner.setSelection(e, true);
                int b = 0;
                for (Ciudad cit : cnt.getCiudades()) {
                    if (cit.getName().equalsIgnoreCase(cityUser)) {
                        Cities.setSelection(b, true);
                        found = true;
                        break;
                    }
                    b++;
                }
            }
            e++;
        }
    }

    private void obtenerDatosUsuario(String id) throws JSONException {
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        requestParams.put(Constantes.TAG_CUSTOMER_ID,id);
        AsyncHttpClientHandler.get("getUserProfile", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                JSONObject content;
                if (response!=null) {
                    try {
                        Log.e("JSONUsuario", response.toString());
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        content = response.getJSONObject(Constantes.TAG_CONTENT);
                        {
                            if (success) {
                                id_usuario          = content.getString(Constantes.TAG_ID);
                                String nameUser     = content.getString(Constantes.TAG_NAME);
                                String phoneUser    = content.getString(Constantes.TAG_PHONE);
                                String usernameUser = content.getString(Constantes.TAG_USERNAME);
                                String emailUser    = content.getString(Constantes.TAG_EMAIL);
                                String sexUser      = content.getString(Constantes.TAG_SEX);
                                String birthUser    = content.getString(Constantes.TAG_BIRTH);
                                String countryId    = content.getString(Constantes.TAG_COUNTRIES_ID);
                                String country      = content.getString(Constantes.TAG_COUNTRY);
                                String citiesId     = content.getString(Constantes.TAG_CITIES_ID);
                                String city         = content.getString(Constantes.TAG_CITY);
                                cities_id = citiesId;

                                Telefono.setText(phoneUser);
                                Username.setText(usernameUser);
                                Email.setText(emailUser);
                                Nombre.setText(nameUser);
                                int position = (sexUser.equalsIgnoreCase("M") ? 1 : 2);
                                Sexo.setSelection(position, true);
                                String[] dateBirth = birthUser.split(" ");
                                Nacimiento.setText(dateBirth[0]);

                                countryUser = country;
                                cityUser    = city;

                                //String sexo = Sexo.getSelectedItem().toString().substring(0,1);

                                info_registro.putString("id",id_usuario);
                                info_registro.putString("nombre",nameUser);
                                info_registro.putString("username",usernameUser);
                                info_registro.putString("telefono",phoneUser);
                                info_registro.putString("email",emailUser);
                                info_registro.putString("ciudad",cities_id);
                                info_registro.putString("fecha",birthUser);
                                info_registro.putString("sexo",sexUser);
                                info_registro.putString("cities_id", citiesId);
                                info_registro.putString("countries_id", countryId);

                                interest = new ArrayList<>();
                                JSONArray intereses = content.getJSONArray(Constantes.TAG_INTEREST);
                                for (int i=0;i<intereses.length();i++){
                                    JSONObject interes = intereses.getJSONObject(i);
                                    interest.add(interes.getString(Constantes.TAG_ID));
                                }

                            }
                        }
                    } catch (JSONException e) {
                    showProgress(false);
                    e.printStackTrace();
                }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject response) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registro1Activity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            obtenerPaises();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void llenarListaPaises(JSONObject response) {
        boolean success;
        JSONArray content;

        if (response != null) {
            try {
                success = response.getBoolean(Constantes.TAG_SUCCESS);
                if (success) {
                    content = response.getJSONArray(Constantes.TAG_CONTENT);
                    paisesList = new LinkedList<>();
                    for (int i = 0; i < content.length(); i++) {
                        JSONObject paises = content.getJSONObject(i);

                        String id_pais = paises.getString(Constantes.TAG_ID);
                        String pais = paises.getString(Constantes.TAG_NAME);

                        LinkedList<Ciudad> ciudadesLinkedList = new LinkedList<>();

                        JSONArray ciudades = paises.getJSONArray(Constantes.TAG_CITIES);
                        for (int j = 0; j < ciudades.length(); j++) {
                            JSONObject ciudad = ciudades.getJSONObject(j);

                            String id_ciudad = ciudad.getString(Constantes.TAG_ID);
                            String nombreCiudad = ciudad.getString(Constantes.TAG_NAME);
                            Ciudad city = new Ciudad(id_ciudad,nombreCiudad);
                            ciudadesLinkedList.add(city);
                        }
                        Pais nuevoPais = new Pais(id_pais,pais,ciudadesLinkedList);
                        paisesList.add(nuevoPais);
                    }

                    ArrayAdapter adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_spinner_item,paisesList);
                    adapter.setDropDownViewResource(R.layout.custom_spinner_item);
                    PaisSpinner.setAdapter(adapter);
                    PaisSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            final LinkedList<Ciudad> ciudadesList = paisesList.get(parent.getSelectedItemPosition()).getCiudades();
                            ArrayAdapter adapter_ciudades = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_spinner_item,ciudadesList);
                            adapter_ciudades.setDropDownViewResource(R.layout.custom_spinner_item);
                            Cities.setAdapter(adapter_ciudades);
                            Cities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    cities_id = ciudadesList.get(parent.getSelectedItemPosition()).getId();
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                    if (Integer.parseInt(id_usuario) > 0) {
                        showCountryUser(countryUser);
                    }
                    showProgress(false);

                } else {
                    showProgress(false);
                    Toast.makeText(getApplicationContext(), "spinner load failed", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                showProgress(false);
                e.printStackTrace();
            }
        } else {
            showProgress(false);
        }
    }

    private void registerProfile(Bundle info) throws JSONException {

        // Reset errors.
        Nombre.setError(null);
        Username.setError(null);
        Password.setError(null);
        Password.setError(null);
        Telefono.setError(null);
        Email.setError(null);
        Nacimiento.setError(null);

        String nombre           = Nombre.getText().toString();
        String username         = Username.getText().toString();
        String password         = Password.getText().toString();
        String passwordConfirm  = PasswordConfirm.getText().toString();
        String telefono         = Telefono.getText().toString();
        String email            = Email.getText().toString();
        String fechaNacimiento  = Nacimiento.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(nombre)) {
            Nombre.setError(getString(R.string.error_field_required));
            focusView = Nombre;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            Username.setError(getString(R.string.error_field_required));
            focusView = Username;
            cancel = true;
        }

        if (TextUtils.isEmpty(telefono)) {
            Telefono.setError(getString(R.string.error_field_required));
            focusView = Telefono;
            cancel = true;
        }

        if (TextUtils.isEmpty(fechaNacimiento)) {
            Nacimiento.setError(getString(R.string.error_field_required));
            focusView = Nacimiento;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            Password.setError(getString(R.string.error_invalid_password));
            focusView = Password;
            cancel = true;
        }

        if (!password.equalsIgnoreCase(passwordConfirm)) {
            Password.setError(getString(R.string.error_no_match_password));
            focusView = Password;
            cancel = true;
        }

        if (TextUtils.isEmpty(password) && Integer.parseInt(id_usuario) == 0) {
            Password.setError(getString(R.string.error_field_required));
            focusView = Password;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            Email.setError(getString(R.string.error_field_required));
            focusView = Email;
            cancel = true;
        } else if (!isEmailValid(email)) {
            Email.setError(getString(R.string.error_invalid_email));
            focusView = Email;
            cancel = true;
        }

        if (Sexo.getSelectedItemPosition()==0){
            cancel = true;
            Toast.makeText(Registro1Activity.this, "Debe seleccionar un sexo", Toast.LENGTH_SHORT).show();
            focusView = Sexo;
        }

        if (cancel) {

            focusView.requestFocus();
        } else {


            ArrayList<String> seleccion = new ArrayList<>();

            String id = info.getString("id");
//            String nombre = info.getString("nombre");
//            String username = info.getString("username");
//            String password = info.getString("password");
//            String telefono = info.getString("telefono");
//            String email = info.getString("email");
//            String ciudad = info.getString("ciudad");
//            String fecha = info.getString("fecha");
//            String sexo = info.getString("sexo");

            String sexo = Sexo.getSelectedItem().toString().substring(0,1);

            RequestParams requestParams = new RequestParams();
            requestParams.put(Constantes.TAG_KEY, Constantes.key);
            requestParams.put(Constantes.TAG_ID, id);
            requestParams.put(Constantes.TAG_NAME, nombre);
            requestParams.put(Constantes.TAG_USERNAME, username);
            requestParams.put(Constantes.TAG_EMAIL, email);
            requestParams.put(Constantes.TAG_PASSWORD, password);
            requestParams.put(Constantes.TAG_PHONE, telefono);
            requestParams.put(Constantes.TAG_CITIES_ID, cities_id);
            requestParams.put(Constantes.TAG_SEX, sexo);
            requestParams.put(Constantes.TAG_BIRTH, fechaNacimiento);
            requestParams.put(Constantes.TAG_CATEGORIES, seleccion);
            requestParams.put(Constantes.TAG_LANGUAGE, "es");

            AsyncHttpClientHandler.post("updateProfile", requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    showProgress(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    boolean success;
                    if (response != null) {
                        try {
                            success = response.getBoolean(Constantes.TAG_SUCCESS);
                            if (success) {
                                showProgress(false);

                                Toast.makeText(getApplicationContext(), response.getString(Constantes.TAG_MESSAGE), Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            } else {

                                Log.e("AsyncHttpClient", "cant get any result");
                                showProgress(false);
                                Toast.makeText(getApplicationContext(), "Register fail: " + response.getString(Constantes.TAG_MESSAGE), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            showProgress(false);
                            e.printStackTrace();
                        }
                    } else {
                        showProgress(false);
                        Log.e("AsyncHttpClient", "cant get any information from URL");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable error) {
                    Log.e("Error", response);
                    showProgress(false);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registro1Activity.this);
                    alertDialog.setTitle("Error de conexion");
                    alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                    alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                registerProfile(info_registro);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog.show();
                }

            });
        }
    }

    private void realizarRegistro() {
        // Reset errors.
        Nombre.setError(null);
        Username.setError(null);
        Password.setError(null);
        Telefono.setError(null);
        Email.setError(null);
        Nacimiento.setError(null);

        String nombre = Nombre.getText().toString();
        String username = Username.getText().toString();
        String password = Password.getText().toString();
        String telefono = Telefono.getText().toString();
        String email = Email.getText().toString();
        String fechaNacimiento = Nacimiento.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(nombre)) {
            Nombre.setError(getString(R.string.error_field_required));
            focusView = Nombre;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            Username.setError(getString(R.string.error_field_required));
            focusView = Username;
            cancel = true;
        }

        if (TextUtils.isEmpty(telefono)) {
            Telefono.setError(getString(R.string.error_field_required));
            focusView = Telefono;
            cancel = true;
        }

        if (TextUtils.isEmpty(fechaNacimiento)) {
            Nacimiento.setError(getString(R.string.error_field_required));
            focusView = Nacimiento;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            Password.setError(getString(R.string.error_invalid_password));
            focusView = Password;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            Password.setError(getString(R.string.error_field_required));
            focusView = Password;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            Email.setError(getString(R.string.error_field_required));
            focusView = Email;
            cancel = true;
        } else if (!isEmailValid(email)) {
            Email.setError(getString(R.string.error_invalid_email));
            focusView = Email;
            cancel = true;
        }

        if (Sexo.getSelectedItemPosition()==0){
            cancel = true;
            Toast.makeText(Registro1Activity.this, "Debe seleccionar un sexo", Toast.LENGTH_SHORT).show();
            focusView = Sexo;
        }

        if (cancel) {

            focusView.requestFocus();
        } else {

            String sexo = Sexo.getSelectedItem().toString().substring(0,1);

            Intent pantalla2 = new Intent(Registro1Activity.this,Registro2Activity.class);
            Bundle info = new Bundle();
            info.putString("id",id_usuario);
            info.putString("nombre",nombre);
            info.putString("username",username);
            info.putString("password",password);
            info.putString("telefono",telefono);
            info.putString("email",email);
            info.putString("ciudad",cities_id);
            info.putString("fecha",fechaNacimiento);
            info.putString("sexo",sexo);
            if (interest!=null)
                info.putStringArrayList("intereses",interest);
            pantalla2.putExtra("info_registro",info);
            startActivity(pantalla2);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return (mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item));
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle("Main Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(mDrawerToggle);
    }

    private void setupNavigationView(){
        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            ((TextView) findViewById(R.id.nombreUsuario)).setText(usuario.getNombre());
            ListView nav_list = (ListView) findViewById(R.id.nav_listView);
            ArrayList<NavItem> nav_items = new ArrayList<>();
            nav_items.add(new NavItem("Categorias", R.drawable.icon_category));
            nav_items.add(new NavItem("Premios", R.drawable.icon_trophy));
            nav_items.add(new NavItem("Perfil", R.drawable.icon_profile));
            nav_items.add(new NavItem("Intereses", R.drawable.icon_favorites_purple));
            nav_items.add(new NavItem("Cerrar Sesion", R.drawable.icon_logout));
            nav_list.setAdapter(new NavListAdapter(getApplicationContext(), nav_items));
            nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            startActivity(new Intent(Registro1Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 1:
                            startActivity(new Intent(Registro1Activity.this, PremioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 2:
                            startActivity(new Intent(Registro1Activity.this,Registro1Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 3:
                            startActivity(new Intent(Registro1Activity.this, Registro2Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 4:
                            PrefUtils.clearUsuario(getApplicationContext());
                            startActivity(new Intent(Registro1Activity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                    }
                }
            });
        }
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mRegistroFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRegistroFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRegistroFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });

    }


}
