package ar.wsti.tipsmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.Categoria;
import ar.wsti.tipsmovil.objetos.NavItem;
import ar.wsti.tipsmovil.objetos.Usuario;
import cz.msebera.android.httpclient.Header;

public class Registro2Activity extends AppCompatActivity {

    private View mProgressView;
    private View mRegistroFormView;
    private DrawerLayout drawer;
    private String mActivityTitle;
    private ActionBarDrawerToggle mDrawerToggle;

    private Bundle info_registro;
    private ListView interesesList;
    private ArrayList<Categoria> categoriaArrayList;
    private ArrayList<String> interest;
    private InteresesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro2);

        mRegistroFormView = findViewById(R.id.registro2Form);
        mProgressView = findViewById(R.id.progress_bar);

        interesesList = (ListView)findViewById(R.id.listaIntereses);
        categoriaArrayList = new ArrayList<>();

        Button finalizar = (Button)findViewById(R.id.finalizarRegistro);
        if (!PrefUtils.getUsuario(getApplicationContext()).getId().equalsIgnoreCase("empty")) {
            info_registro = new Bundle();
            finalizar.setText(R.string.actualizar);
            try {
                obtenerDatosUsuario(PrefUtils.getUsuario(getApplicationContext()).getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            finalizar.setText(R.string.finalizar);
            Intent intent = getIntent();
            info_registro = intent.getBundleExtra("info_registro");
            interest = info_registro.getStringArrayList("intereses");
            try{
                obtenerCategorias();
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        mActivityTitle = getTitle().toString();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_main);

        setupDrawer();
        setupNavigationView();


        Button modal = (Button)findViewById(R.id.modal);
        modal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Registro2Activity.this,ModalActivity.class));
            }
        });

        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    RegisterProfile(info_registro);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

    }

    private void obtenerDatosUsuario(String id) throws JSONException {
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        requestParams.put(Constantes.TAG_CUSTOMER_ID,id);
        AsyncHttpClientHandler.get("getUserProfile", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                JSONObject content;
                if (response!=null) {
                    try {
                        Log.e("JSONUsuario", response.toString());
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        content = response.getJSONObject(Constantes.TAG_CONTENT);
                        {
                            if (success) {
                                String userId       = content.getString(Constantes.TAG_ID);
                                String nameUser     = content.getString(Constantes.TAG_NAME);
                                String phoneUser    = content.getString(Constantes.TAG_PHONE);
                                String usernameUser = content.getString(Constantes.TAG_USERNAME);
                                String emailUser    = content.getString(Constantes.TAG_EMAIL);
                                String sexUser      = content.getString(Constantes.TAG_SEX);
                                String birthUser    = content.getString(Constantes.TAG_BIRTH);
                                String countryId    = content.getString(Constantes.TAG_COUNTRIES_ID);
                                String citiesId     = content.getString(Constantes.TAG_CITIES_ID);
                                interest = new ArrayList<>();
                                JSONArray intereses = content.getJSONArray(Constantes.TAG_INTEREST);
                                for (int i=0;i<intereses.length();i++){
                                    JSONObject interes = intereses.getJSONObject(i);
                                    interest.add(interes.getString(Constantes.TAG_ID));
                                }

                                info_registro.putString("id",userId);
                                info_registro.putString("nombre",nameUser);
                                info_registro.putString("username",usernameUser);
                                info_registro.putString("telefono",phoneUser);
                                info_registro.putString("email",emailUser);
                                info_registro.putString("ciudad",citiesId);
                                info_registro.putString("fecha",birthUser);
                                info_registro.putString("sexo",sexUser);
                                info_registro.putString("countries_id", countryId);
                                info_registro.putStringArrayList("intereses",interest);
                                //interest = info_registro.getStringArrayList("intereses");


                                obtenerCategorias();

                            }
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject response) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registro2Activity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            obtenerDatosUsuario(PrefUtils.getUsuario(getApplicationContext()).getId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }
        });
    }


    public void obtenerCategorias() throws JSONException {
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        AsyncHttpClientHandler.get("getFidelidadCategories", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                JSONArray content;
                if (response != null) {
                    try {
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        if (success) {
                            content = response.getJSONArray(Constantes.TAG_CONTENT);
                            for (int i = 0;i<content.length();i++) {
                                JSONObject c = content.getJSONObject(i);

                                String id = c.getString(Constantes.TAG_ID);
                                String nombre = c.getString(Constantes.TAG_NAME);
                                String foto = c.getString(Constantes.TAG_PHOTO);
                                Categoria categoria = new Categoria(id,nombre,foto);
                                categoriaArrayList.add(categoria);
                            }
                            showProgress(false);
                            boolean[] interesesChecked;
                            interesesChecked = new boolean[categoriaArrayList.size()];
                            if (interest!=null) {
                                for (int i = 0; i < categoriaArrayList.size(); i++) {
                                    for (String id : interest) {
                                        if (categoriaArrayList.get(i).getId().equals(id)) {
                                            interesesChecked[i] = true;
                                            break;
                                        } else {
                                            interesesChecked[i] = false;
                                        }
                                    }
                                }
                            } else {
                                for (int i = 0; i < categoriaArrayList.size(); i++) {
                                    interesesChecked[i] = true;
                                }
                            }

                            adapter = new InteresesAdapter(Registro2Activity.this,categoriaArrayList,interesesChecked);
                            interesesList.setAdapter(adapter);

                        } else {
                            Log.e("AsyncHttpClient", "cant get any result");
                            showProgress(false);
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                } else {
                    showProgress(false);
                    Log.e("AsyncHttpClient", "cant get any information from URL");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseError) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registro2Activity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            obtenerCategorias();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }

            @Override
            public void onCancel(){
                finish();
            }

        });

    }



    private void RegisterProfile(Bundle info) throws JSONException {

        boolean[] checked = adapter.getSeleccion();
        ArrayList<String> seleccion = new ArrayList<>();

        for (int i= 0; i < categoriaArrayList.size(); i++) {
            Categoria categoria = categoriaArrayList.get(i);
            if (checked[i]){
                seleccion.add(categoria.getId());
            }
        }

        String id = info.getString("id");
        String nombre = info.getString("nombre");
        String username = info.getString("username");
        String password = info.getString("password");
        String telefono = info.getString("telefono");
        String email = info.getString("email");
        String ciudad = info.getString("ciudad");
        String fecha = info.getString("fecha");
        String sexo = info.getString("sexo");


        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY, Constantes.key);
        requestParams.put(Constantes.TAG_ID, id);
        requestParams.put(Constantes.TAG_NAME, nombre);
        requestParams.put(Constantes.TAG_USERNAME, username);
        requestParams.put(Constantes.TAG_EMAIL, email);
        requestParams.put(Constantes.TAG_PASSWORD, "");
        requestParams.put(Constantes.TAG_PHONE, telefono);
        requestParams.put(Constantes.TAG_CITIES_ID,ciudad);
        requestParams.put(Constantes.TAG_SEX,sexo);
        requestParams.put(Constantes.TAG_BIRTH,fecha);
        requestParams.put(Constantes.TAG_CATEGORIES,seleccion);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");

        AsyncHttpClientHandler.post("register", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                if (response != null) {
                    try {
                        Log.i("JSONRegistro", response.toString());
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        if (success) {
                            showProgress(false);

                            Toast.makeText(getApplicationContext(), response.getString(Constantes.TAG_MESSAGE), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }else {

                            Log.e("AsyncHttpClient", "cant get any result");
                            showProgress(false);
                            Toast.makeText(getApplicationContext(), "Register fail: " + response.getString(Constantes.TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                } else {
                    showProgress(false);
                    Log.e("AsyncHttpClient", "cant get any information from URL");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable error) {
                Log.e("Error",response);
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registro2Activity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            RegisterProfile(info_registro);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }

        });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.right_in,R.anim.right_out);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_in,R.anim.left_out);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle("Main Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(mDrawerToggle);
    }

    private void setupNavigationView(){
        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            ((TextView) findViewById(R.id.nombreUsuario)).setText(usuario.getNombre());
            ListView nav_list = (ListView) findViewById(R.id.nav_listView);
            ArrayList<NavItem> nav_items = new ArrayList<>();
            nav_items.add(new NavItem("Categorias", R.drawable.icon_category));
            nav_items.add(new NavItem("Premios", R.drawable.icon_trophy));
            nav_items.add(new NavItem("Perfil", R.drawable.icon_profile));
            nav_items.add(new NavItem("Cerrar Sesion", R.drawable.icon_logout));
            nav_list.setAdapter(new NavListAdapter(getApplicationContext(), nav_items));
            nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            startActivity(new Intent(Registro2Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 1:
                            startActivity(new Intent(Registro2Activity.this, PremioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 2:
                            startActivity(new Intent(Registro2Activity.this,Registro1Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 3:
                            PrefUtils.clearUsuario(getApplicationContext());
                            startActivity(new Intent(Registro2Activity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                    }
                }
            });
        }
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mRegistroFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRegistroFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRegistroFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });

    }
}
