package ar.wsti.tipsmovil.activities;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.ArrayList;

import ar.wsti.tipsmovil.LoginActivity;
import ar.wsti.tipsmovil.MainActivity;
import ar.wsti.tipsmovil.NavListAdapter;
import ar.wsti.tipsmovil.PrefUtils;
import ar.wsti.tipsmovil.PremioActivity;
import ar.wsti.tipsmovil.R;
import ar.wsti.tipsmovil.Registro1Activity;
import ar.wsti.tipsmovil.Registro2Activity;
import ar.wsti.tipsmovil.objetos.NavItem;
import ar.wsti.tipsmovil.objetos.Usuario;

public class MarketingActivity extends AppCompatActivity {

    String id;
    String name;
    String photo;
    String pub_id;
    Double distance;
    String video;
    int expo_time;
    String texto1;
    String texto2;
    Double latitude;
    Double longitude;
    Double far;
    String website;
    String description;

    MediaPlayer mediaPlayer;
    boolean isPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing);

        Intent detalle = getIntent();
        Bundle bundle = detalle.getBundleExtra("detalles");

        id       = bundle.getString("id");
        name     = bundle.getString("name");
        photo    = bundle.getString("photo");
        pub_id   = bundle.getString("pub_id");
        distance = bundle.getDouble("distance");
        video    = bundle.getString("video");
        expo_time  = bundle.getInt("expo_time");
        texto1   = bundle.getString("texto1");
        texto2   = bundle.getString("texto2");
        latitude = bundle.getDouble("latitude");
        longitude = bundle.getDouble("longitude");
        far      = bundle.getDouble("far");
        website  = bundle.getString("website");
        description = bundle.getString("description");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setTitle("Mensaje Marketing");
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_marketing);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        setupNavigationView();

        TextView nameText = (TextView) findViewById(R.id.marketing_name);
        ImageView photoImage = (ImageView) findViewById(R.id.marketing_image);
        TextView texto1Text  = (TextView) findViewById(R.id.marketing_text1);
        TextView texto2Text  = (TextView) findViewById(R.id.marketing_text2);
        TextView webView     = (TextView) findViewById(R.id.marketing_description);
        WebView  videoView   = (WebView) findViewById(R.id.marketing_video);
        Button buttonSound   = (Button) findViewById(R.id.marketing_sound);
        Button buttonShare   = (Button) findViewById(R.id.marketing_share);

        photoImage.setVisibility(View.GONE);
        nameText.setVisibility(View.GONE);
        texto1Text.setVisibility(View.GONE);
        texto2Text.setVisibility(View.GONE);
        webView.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        buttonSound.setVisibility(View.GONE);
        buttonShare.setVisibility(View.GONE);

        switch (Integer.parseInt(pub_id)) {
            case 1:
                videoView.setVisibility(View.VISIBLE);
                String[] videoId = website.split("/");
                String frameVideo = "<html><body><iframe width=\"420\" height=\"315\" src=\"https://www.youtube.com/embed/"+videoId[3]+"/?&playsinline=1\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
                videoView.loadData(frameVideo, "text/html", "utf-8");
                break;
            case 2:
                photoImage.setVisibility(View.VISIBLE);
                Glide.with(photoImage.getContext()).load(photo).placeholder(R.drawable.logo).into(photoImage);
                break;
            case 3:
                webView.setVisibility(View.VISIBLE);
                webView.setText(Html.fromHtml(website,0));
                break;
            case 4:
                photoImage.setVisibility(View.VISIBLE);
                texto1Text.setVisibility(View.VISIBLE);
                texto2Text.setVisibility(View.VISIBLE);

                Glide.with(photoImage.getContext()).load(photo).into(photoImage);
                texto1Text.setText(texto1);
                texto2Text.setText(texto2);
                break;
            case 5:
                nameText.setVisibility(View.VISIBLE);
                webView.setVisibility(View.VISIBLE);
                nameText.setText(name);
                webView.setText(Html.fromHtml(description, 0));
                break;
            case 6:
                Uri myUri = Uri.parse(website);
                buttonSound.setVisibility(View.VISIBLE);
                buttonSound.setText("Play / Stop");
                try {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(this, myUri);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                buttonSound.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isPlaying) {
                            try {
                                isPlaying = true;
                                mediaPlayer.prepare();
                                mediaPlayer.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            isPlaying = false;
                            mediaPlayer.release();
                            mediaPlayer.stop();
                            mediaPlayer = null;
                        }
                    }
                });
                break;
            case 7:
                texto1Text.setVisibility(View.VISIBLE);
                buttonShare.setVisibility(View.VISIBLE);
                texto1Text.setText(website);
                buttonShare.setText("Compartir Documento");
                buttonShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        String shareBody = website;
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Te invito a que veas este documento");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, website);
                        startActivity(Intent.createChooser(sharingIntent, "Compartido a través de Tips Móvil WSTI Argentina"));
                    }
                });
                break;
        }

    }

    @Override
    protected void onDestroy() {
        if (isPlaying) {
            isPlaying = false;
            mediaPlayer.release();
            mediaPlayer.stop();
            mediaPlayer = null;
        }
        super.onDestroy();
    }

    private void setupNavigationView() {
        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_marketing);
        if (navigationView != null) {
            ((TextView) findViewById(R.id.nombreUsuario)).setText(usuario.getNombre());
            ListView nav_list = (ListView) findViewById(R.id.nav_listViewMarketing);
            ArrayList<NavItem> nav_items = new ArrayList<>();
            nav_items.add(new NavItem("Categorias", R.drawable.icon_category));
            nav_items.add(new NavItem("Premios", R.drawable.icon_trophy));
            nav_items.add(new NavItem("Perfil", R.drawable.icon_profile));
            nav_items.add(new NavItem("Intereses", R.drawable.icon_favorites_purple));
            nav_items.add(new NavItem("Cerrar Sesion", R.drawable.icon_logout));
            nav_list.setAdapter(new NavListAdapter(getApplicationContext(), nav_items));
            nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            startActivity(new Intent(MarketingActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 1:
                            startActivity(new Intent(MarketingActivity.this, PremioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 2:
                            startActivity(new Intent(MarketingActivity.this, Registro1Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 3:
                            startActivity(new Intent(MarketingActivity.this, Registro2Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 4:
                            PrefUtils.clearUsuario(getApplicationContext());
                            startActivity(new Intent(MarketingActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_marketing);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detalle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
