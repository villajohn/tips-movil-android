package ar.wsti.tipsmovil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.Categoria;

public class CategoriasAdapter extends BaseAdapter{

    private final Context mContext;
    private final ArrayList<Categoria> items;
    private OnCategoriaSeleccionadoListener listener;

    public CategoriasAdapter(Context c, ArrayList<Categoria> items, OnCategoriaSeleccionadoListener listener) {
        mContext = c;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Categoria getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.categorias_item, viewGroup, false);
        }

        Categoria item = getItem(position);

        final String id = item.getId();

        final String nombre = item.getName();
        ImageView image = (ImageView) view.findViewById(R.id.imagenCat);
        Glide.with(image.getContext()).load(item.getPhoto()).into(image);

        TextView name = (TextView) view.findViewById(R.id.nombreCat);
        name.setText(nombre);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnCategoriaSeleccionado(id,nombre);
            }
        });

        return view;
    }

    public interface OnCategoriaSeleccionadoListener {
        void OnCategoriaSeleccionado(String id,String nombre);
    }
}
