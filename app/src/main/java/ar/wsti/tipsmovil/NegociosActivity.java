package ar.wsti.tipsmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.NavItem;
import ar.wsti.tipsmovil.objetos.Negocio;
import ar.wsti.tipsmovil.objetos.Usuario;
import cz.msebera.android.httpclient.Header;

public class NegociosActivity extends AppCompatActivity{

    private ProgressDialog pDialog;
    private View negocioFormView;
    private ListView negociosListView;
    private NegociosAdapter adapter;
    private DrawerLayout drawer;
    private String mActivityTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<Negocio> NegociosArrayList;
    private NegociosAdapter.OnItemSelectedListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_negocios);

        Intent intent = getIntent();
        String categoria_id = intent.getStringExtra("id");
        mActivityTitle = intent.getStringExtra("nombre");

        NegociosArrayList = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            if (getSupportActionBar() !=null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setTitle(mActivityTitle);
            }
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_main);

        setupDrawer();
        setupNavigationView();

        pDialog = new ProgressDialog(NegociosActivity.this);
        pDialog.setMessage("Cargando contenido...");
        pDialog.setCancelable(false);

        negocioFormView = findViewById(R.id.negociosFormView);
        negociosListView = (ListView) findViewById(R.id.negociosListView);

        listener = new NegociosAdapter.OnItemSelectedListener() {
            @Override
            public void OnItemSelected(Negocio negocio) {
                Intent detalle = new Intent(NegociosActivity.this,DetalleActivity.class);
                Bundle bundle = new Bundle();

                bundle.putString("name",negocio.getName());
                bundle.putString("description",negocio.getDescription());
                bundle.putString("photo",negocio.getPhoto());
                bundle.putString("latitude",negocio.getLatitude());
                bundle.putString("longitude",negocio.getLongitude());
                bundle.putString("address",negocio.getAddress());
                bundle.putString("phone",negocio.getPhone());
                bundle.putString("email",negocio.getEmail());
                bundle.putString("instagram",negocio.getInstagram());
                bundle.putString("facebook",negocio.getFacebook());
                bundle.putString("twitter",negocio.getCity());

                detalle.putExtra("detalles",bundle);
                startActivity(detalle);
            }
        };

        try{
            obtenerNegocios(categoria_id);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    private void obtenerNegocios(String id) throws JSONException {
        final String mId = id;
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        requestParams.put(Constantes.TAG_CATEGORY,id);
        AsyncHttpClientHandler.get("getBusinessesByCategory", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                getInformacionNegocios(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error,JSONObject responseError) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(NegociosActivity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            obtenerNegocios(mId);
                        }catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }

            @Override
            public void onCancel(){
                finish();
            }

        });
    }

    private void getInformacionNegocios(JSONObject response) {
        boolean success;
        JSONArray content;
        if (response != null) {
            try {
                Log.d("JSON negocio",response.toString());
                success = response.getBoolean(Constantes.TAG_SUCCESS);
                if (success) {

                    content = response.getJSONArray(Constantes.TAG_CONTENT);
                    for (int i = 0;i<content.length();i++) {
                        JSONObject c = content.getJSONObject(i);

                        Negocio negocio = new Negocio();
                        negocio.setId(c.getString(Constantes.TAG_ID));
                        negocio.setName(c.getString(Constantes.TAG_NAME));
                        negocio.setDescription(c.getString(Constantes.TAG_DESCRIPTION));
                        negocio.setEmail(c.getString(Constantes.TAG_EMAIL));
                        negocio.setLatitude(c.getString(Constantes.TAG_LATITUDE));
                        negocio.setLongitude(c.getString(Constantes.TAG_LONGITUDE));
                        negocio.setFacebook(c.getString(Constantes.TAG_FACEBOOK));
                        negocio.setTwitter(c.getString(Constantes.TAG_TWITTER));
                        negocio.setInstagram(c.getString(Constantes.TAG_INSTAGRAM));
                        negocio.setAddress(c.getString(Constantes.TAG_ADDRESS));
                        negocio.setCity(c.getString(Constantes.TAG_CITY));
                        negocio.setPhone(c.getString(Constantes.TAG_PHONE));
                        negocio.setPhoto(c.getString(Constantes.TAG_PHOTO));

                        NegociosArrayList.add(negocio);
                    }
                    showProgress(false);
                    adapter = new NegociosAdapter(getApplicationContext(),NegociosArrayList,listener,1);
                    negociosListView.setAdapter(adapter);
                    setupSearch();

                } else {
                    showProgress(false);
                }
            } catch (JSONException e) {
                showProgress(false);
                e.printStackTrace();
            }
        } else {
            showProgress(false);
        }
    }

    private void setupSearch(){
        EditText search = (EditText) findViewById(R.id.buscar);
        if (search!= null) {
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    if (arg0.length() > 0)
                        NegociosActivity.this.adapter.getFilter().filter(arg0);
                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    if (arg0.length() == 0) {
                        adapter = new NegociosAdapter(getApplicationContext(),NegociosArrayList,listener,1);
                        negociosListView.setAdapter(adapter);
                        NegociosActivity.this.adapter.getFilter().filter(arg0);
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.right_in,R.anim.right_out);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_in,R.anim.left_out);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle("Main Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(mDrawerToggle);
    }

    private void setupNavigationView(){
        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            ((TextView) findViewById(R.id.nombreUsuario)).setText(usuario.getNombre());
            ListView nav_list = (ListView) findViewById(R.id.nav_listView);
            ArrayList<NavItem> nav_items = new ArrayList<>();
            nav_items.add(new NavItem("Categorias", R.drawable.icon_category));
            nav_items.add(new NavItem("Premios", R.drawable.icon_trophy));
            nav_items.add(new NavItem("Perfil", R.drawable.icon_profile));
            nav_items.add(new NavItem("Cerrar Sesion", R.drawable.icon_logout));
            nav_list.setAdapter(new NavListAdapter(getApplicationContext(), nav_items));
            nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            startActivity(new Intent(NegociosActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 1:
                            startActivity(new Intent(NegociosActivity.this, PremioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 2:
                            startActivity(new Intent(NegociosActivity.this,Registro1Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 3:
                            PrefUtils.clearUsuario(getApplicationContext());
                            startActivity(new Intent(NegociosActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                    }
                }
            });
        }
    }


    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        negocioFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        negocioFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                negocioFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        if (show)
            pDialog.show();
        else
            pDialog.dismiss();

    }
}
