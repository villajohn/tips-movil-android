package ar.wsti.tipsmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import ar.wsti.tipsmovil.objetos.Usuario;
import cz.msebera.android.httpclient.Header;


public class LoginActivity extends AppCompatActivity implements RecuperarPasswordFragment.OnEnviarCorreoListener {

    private static final int REQUEST_CODE_FB = 64206;
    private static final int NORMAL_LOGIN = 1;
    private static final int SOCIAL_LOGIN = 2;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        if (!(usuario.getNombre().equals("Empty") && usuario.getEmail().equals("Empty") && usuario.getId().equals("Empty"))){
            Toast.makeText(getApplicationContext(), "Bienvenido " + usuario.getNombre(), Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }

        //initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.email_login_form);
        mProgressView = findViewById(R.id.progress_bar);

        Button registrar = (Button)findViewById(R.id.registrarBoton);
        if (registrar != null)
            registrar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),Registro1Activity.class));
                    overridePendingTransition(R.anim.left_in,R.anim.left_out);
                }
            });

        Button olvido = (Button)findViewById(R.id.olvidoBoton);
        olvido.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                RecuperarPasswordFragment alert = new RecuperarPasswordFragment();
                alert.show(getFragmentManager(),"alert_dialog_radio");
            }
        });

        final LoginButton loginButton = (LoginButton) findViewById(R.id.fb_login_button);
        if (loginButton !=null) {
            loginButton.setReadPermissions(Arrays.asList("email","public_profile"));
            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    FacebookLogin(loginResult);
                }

                @Override
                public void onCancel() {
                    Toast.makeText(getApplicationContext(),"Facebook login cancelled",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(getApplicationContext(),"Has occurred an error", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    public void onCorreoEnviado(String email){
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_EMAIL,email);
        AsyncHttpClientHandler.post("forgotPassword", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                if (response != null) {
                    try {
                        Log.d("Recuperar Contraseña",response.toString());
                        success = response.getBoolean(Constantes.TAG_SUCCESS);

                        showProgress(false);
                        Toast.makeText(LoginActivity.this, response.getString(Constantes.TAG_MESSAGE), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                } else {
                    showProgress(false);
                    Log.e("AsyncHttpClient", "cant get any information from URL");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseError) {
                showProgress(false);
                Toast.makeText(LoginActivity.this, "Hubo un fallo al enviar su solicitud", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel(){
                finish();
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data) {
        if (requestCode == REQUEST_CODE_FB)
            callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode,resultCode,data);
    }

    private void FacebookLogin(LoginResult result){
        if(result != null) {
            AccessToken token = result.getAccessToken();
            String id = token.getUserId();
            GraphRequest request = GraphRequest.newMeRequest(token,new GraphRequest.GraphJSONObjectCallback(){
                @Override
                public void onCompleted(JSONObject object, GraphResponse response){
                    try {
                        Log.i("Response",object.toString());
                        String email = object.getString("email");
                        String firstName = object.getString("first_name");
                        String lastName = object.getString("last_name");

                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields","email,first_name,last_name");
            request.setParameters(parameters);
            request.executeAsync();
        }
    }


    private void attemptLogin() {

        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            try {
                iniciarSesion(NORMAL_LOGIN,email, password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void iniciarSesion(final int method, final String e_mail, final String password) throws JSONException {

        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY, Constantes.key);
        requestParams.put(Constantes.TAG_EMAIL, e_mail);
        requestParams.put(Constantes.TAG_PASSWORD, password);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        AsyncHttpClientHandler.post("login", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                String message;
                if (response != null) {
                    try {
                        Log.i("JSONLogin", response.toString());
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        message = Constantes.TAG_MESSAGE;
                        if (success) {
                            showProgress(false);

                            JSONObject content = response.getJSONObject(Constantes.TAG_CONTENT);

                            String id = content.getString(Constantes.TAG_ID);
                            String nombre = content.getString(Constantes.TAG_NAME);
                            String email = content.getString(Constantes.TAG_EMAIL);

                            PrefUtils.setUsuario(new Usuario(id,nombre,email),getApplicationContext());

                            Toast.makeText(getApplicationContext(), response.getString(message), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), response.getString(message), Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                } else {
                    showProgress(false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject response) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            iniciarSesion(method,e_mail, password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }

        });
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }


    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });

    }
}

