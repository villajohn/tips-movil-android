package ar.wsti.tipsmovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ModalActivity extends AppCompatActivity {

    private View mProgressView;
    private WebView mHelpData;
    private View mContentView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modal);
        mProgressView = findViewById(R.id.progress_bar_modal);
        mContentView = findViewById(R.id.modalInterestHelpView);
        mHelpData = (WebView) findViewById(R.id.interestHelpContent);
        progressDialog = new ProgressDialog(this);

        try {
            getInterestsHelp();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getInterestsHelp() throws JSONException {
        showProgress(true);
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        AsyncHttpClientHandler.get("getInterestsHelp", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                if (response!=null) {
                    try {
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        if (success) {
                            String contentHelp = response.getString(Constantes.TAG_CONTENT);
                            mHelpData.loadData(contentHelp, "text/html", "UTF-8");
                        } else {
                            Toast.makeText(ModalActivity.this, response.getString(Constantes.TAG_MESSAGE), Toast.LENGTH_SHORT).show();
                        }
                        showProgress(false);

                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject response) {
                showProgress(false);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ModalActivity.this);
                alertDialog.setTitle("Error de conexion");
                alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            getInterestsHelp();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mContentView.setVisibility(show ? View.GONE : View.VISIBLE);
        mContentView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mContentView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });

        if (show) {
            progressDialog.setMessage("espere...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        } else {
            progressDialog.hide();
        }

    }

}
