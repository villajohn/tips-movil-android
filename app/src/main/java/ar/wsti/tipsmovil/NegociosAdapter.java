package ar.wsti.tipsmovil;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Locale;

import ar.wsti.tipsmovil.objetos.Negocio;

public class NegociosAdapter extends ArrayAdapter implements Filterable {

    private static final int NEGOCIO = 1;
    private static final int PREMIO = 2;
    private ArrayList<Negocio> items;
    private Context mContext;
    private OnItemSelectedListener mListener;
    private int tipoItem;

    public NegociosAdapter(Context context, ArrayList<Negocio> items, OnItemSelectedListener listener, int tipoItem) {
        super(context, 0, items);
        mContext = context;
        this.items = items;
        this.mListener = listener;
        this.tipoItem = tipoItem;
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View listItemView;

        if (null == convertView) {
            if (tipoItem == NEGOCIO) {
                listItemView = layoutInflater.inflate(
                        R.layout.negocio_item,
                        parent,
                        false);
            }else{
                listItemView = layoutInflater.inflate(
                        R.layout.premio_item,
                        parent,
                        false);
            }

        } else listItemView = convertView;

        final Negocio negocio = items.get(position);

        if(getCount()!=0){
            if(tipoItem==NEGOCIO) {
                ImageView logo = (ImageView) listItemView.findViewById(R.id.negocioItemLogo);
                TextView nombre = (TextView) listItemView.findViewById(R.id.negocioItemNombre);
                TextView direccion = (TextView) listItemView.findViewById(R.id.negocioItemDireccion);


                Glide.with(mContext)
                        .load(negocio.getPhoto())
                        //.placeholder(R.drawable.logo)
                        .fitCenter()
                        .into(logo);

                nombre.setText(negocio.getName());
                direccion.setText(negocio.getAddress());


            }else{
                ImageView logo = (ImageView) listItemView.findViewById(R.id.premioItemLogo);
                TextView nombre = (TextView) listItemView.findViewById(R.id.premioItemNombre);

                Glide.with(mContext)
                        .load(negocio.getPhoto())
                        //.placeholder(R.drawable.logo)
                        .fitCenter()
                        .into(logo);

                nombre.setText(negocio.getName());
            }
            listItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.OnItemSelected(negocio);
                }
            });

        }

        return listItemView;
    }

        @Override
        public Filter getFilter() {
            return new Filter() {

                @Override
                protected Filter.FilterResults performFiltering(CharSequence constraint) {
                    Filter.FilterResults filterResults = new Filter.FilterResults();
                    ArrayList<Negocio> tempList = new ArrayList<>();
                    try {
                        if (constraint != null) {
                            Locale locale = Locale.ENGLISH;
                            constraint = constraint.toString().toLowerCase(locale);
                            int i = 0;
                            int length;
                            length = items.size();
                            while (i < length) {
                                Negocio item = items.get(i);
                                String data = item.toString();
                                if (data.toLowerCase(locale).contains(constraint.toString().toLowerCase().trim())) {
                                    tempList.add(item);
                                }
                                i++;
                            }
                            filterResults.values = tempList;
                            filterResults.count = tempList.size();
                        }
                    } catch (Exception e) {
                        Toast.makeText(mContext,e.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return filterResults;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, Filter.FilterResults results) {

                    try {
                        items = (ArrayList<Negocio>) results.values;
                        if (results.count > 0) {
                            notifyDataSetChanged();
                        } else {
                            notifyDataSetChanged();
                        }
                    }catch (Exception e) {
                        Log.d("Error: ",e.toString());
                    }
                }

            };
        }

    public interface OnItemSelectedListener {
        void OnItemSelected(Negocio negocio);
    }

}
