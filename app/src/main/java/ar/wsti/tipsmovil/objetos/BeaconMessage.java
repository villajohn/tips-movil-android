package ar.wsti.tipsmovil.objetos;

/**
 * Created by jhons on 12/8/16.
 */
public class BeaconMessage {

    private String mId;
    private String mName;
    private String mPhoto;
    private String mPublicationId;
    private Double mDistance;
    private String mVideoUrl;
    private int mExpoTime;
    private String mTexto1;
    private String mTexto2;
    private Double mLatitude;
    private Double mLongitude;
    private Double mFar;
    private String mWebsite;
    private String mDescription;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPhoto() {
        return mPhoto;
    }

    public void setmPhoto(String mPhoto) {
        this.mPhoto = mPhoto;
    }

    public String getmPublicationId() {
        return mPublicationId;
    }

    public void setmPublicationId(String mPublicationId) {
        this.mPublicationId = mPublicationId;
    }

    public Double getmDistance() {
        return mDistance;
    }

    public void setmDistance(Double mDistance) {
        this.mDistance = mDistance;
    }

    public String getmVideoUrl() {
        return mVideoUrl;
    }

    public void setmVideoUrl(String mVideoUrl) {
        this.mVideoUrl = mVideoUrl;
    }

    public int getmExpoTime() {
        return mExpoTime;
    }

    public void setmExpoTime(int mExpoTime) {
        this.mExpoTime = mExpoTime;
    }

    public String getmTexto1() {
        return mTexto1;
    }

    public void setmTexto1(String mTexto1) {
        this.mTexto1 = mTexto1;
    }

    public String getmTexto2() {
        return mTexto2;
    }

    public void setmTexto2(String mTexto2) {
        this.mTexto2 = mTexto2;
    }

    public Double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public Double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public Double getmFar() {
        return mFar;
    }

    public void setmFar(Double mFar) {
        this.mFar = mFar;
    }

    public String getmWebsite() {
        return mWebsite;
    }

    public void setmWebsite(String mWebsite) {
        this.mWebsite = mWebsite;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

}
