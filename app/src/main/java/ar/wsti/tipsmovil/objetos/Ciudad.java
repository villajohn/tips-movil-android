package ar.wsti.tipsmovil.objetos;

public class Ciudad {
    private String id;
    private String name;

    public Ciudad(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
