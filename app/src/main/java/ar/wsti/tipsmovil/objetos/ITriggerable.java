package ar.wsti.tipsmovil.objetos;

import java.util.List;

/**
 * Created by jhons on 12/15/16.
 */
public interface ITriggerable {
    List<ActionBeacon> getActions();

    void addAction(ActionBeacon action);

    void addActions(List<ActionBeacon> actions);
}
