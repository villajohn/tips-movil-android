package ar.wsti.tipsmovil.objetos;

import java.util.LinkedList;

import ar.wsti.tipsmovil.objetos.Ciudad;

public class Pais {
    private String id;
    private String name;
    private LinkedList<Ciudad> ciudades;

    public Pais(String id, String name, LinkedList<Ciudad> ciudades){
        this.id = id;
        this.name = name;
        this.ciudades = ciudades;
    }

    public String toString(){
        return name;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public LinkedList<Ciudad> getCiudades(){
        return ciudades;
    }
}
