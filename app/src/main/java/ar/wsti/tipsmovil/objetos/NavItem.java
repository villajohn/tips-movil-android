package ar.wsti.tipsmovil.objetos;

public class NavItem {
    private String nombre;
    private int imagen;

    public NavItem(String nombre,int imagen){
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public int getImagen() {
        return imagen;
    }
}
