package ar.wsti.tipsmovil.objetos;

public class Categoria {
    String id;
    String name;
    String photo;

    public Categoria(String id,String name,String photo){
        this.id = id;
        this.name = name;
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

}
