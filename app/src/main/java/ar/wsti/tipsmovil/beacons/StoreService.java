package ar.wsti.tipsmovil.beacons;

import java.util.List;

import ar.wsti.tipsmovil.objetos.ActionBeacon;
import ar.wsti.tipsmovil.objetos.TrackedBeacon;

/**
 * Created by jhons on 12/15/16.
 */
public interface StoreService {

    boolean createBeacon(final TrackedBeacon beacon);

    boolean updateBeacon(final TrackedBeacon beacon);

    boolean deleteBeacon(final String id, boolean cascade);

    TrackedBeacon getBeacon(final String id);

    List<TrackedBeacon> getBeacons();

    boolean updateBeaconDistance(final String id, double distance);

    boolean updateBeaconAction(ActionBeacon beacon);

    boolean createBeaconAction(ActionBeacon beacon);

    List<ActionBeacon> getBeaconActions(final String beaconId);

    boolean deleteBeaconAction(final int id);

    boolean deleteBeaconActions(final String beaconId);

    List<ActionBeacon> getAllEnabledBeaconActions();

    boolean updateBeaconActionEnable(final int id, boolean enable);

    List<ActionBeacon> getEnabledBeaconActionsByEvent(final int eventType, final String beaconId);
}
