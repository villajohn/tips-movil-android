package ar.wsti.tipsmovil.beacons;

import android.app.Application;

import org.altbeacon.beacon.BeaconManager;

import javax.inject.Singleton;

import ar.wsti.tipsmovil.injection.module.ApplicationModule;
import dagger.Component;

/**
 * Created by jhons on 12/15/16.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Application application();

    DataManager dataManager();

    BeaconManager beaconManager();

    ActionExecutor actionExecutor();
}
