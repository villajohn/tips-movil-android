package ar.wsti.tipsmovil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.Categoria;

public class InteresesAdapter extends BaseAdapter{

    private final Context mContext;
    private final ArrayList<Categoria> items;
    private boolean[] interesesSeleccionados;
    private ViewHolder viewHolder;

    public InteresesAdapter(Context c, ArrayList<Categoria> items,boolean[] interesesSeleccionados) {
        mContext = c;
        this.items = items;
        if (interesesSeleccionados == null)
            this.interesesSeleccionados = new boolean[this.items.size()];
        else
            this.interesesSeleccionados = interesesSeleccionados;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Categoria getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final int mPosition = position;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.intereses_item, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.nombre = (TextView) view.findViewById(R.id.interesNombre);
            viewHolder.aSwitch = (Switch) view.findViewById(R.id.interesSwitch);
            view.setTag(R.layout.intereses_item,viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag(R.layout.intereses_item);
        }

        Categoria item = getItem(position);

        viewHolder.nombre.setText(item.getName());

        viewHolder.aSwitch.setOnCheckedChangeListener(null);
        viewHolder.aSwitch.setChecked(interesesSeleccionados[position]);
        viewHolder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                interesesSeleccionados[mPosition] = b;
            }
        });

        return view;
    }

    public boolean[] getSeleccion(){
        return interesesSeleccionados;
    }

    public final class ViewHolder {
        TextView nombre;
        Switch aSwitch;
    }
}


