package ar.wsti.tipsmovil;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.NavItem;
import ar.wsti.tipsmovil.objetos.Usuario;

public class DetalleActivity extends AppCompatActivity implements OnMapReadyCallback {

    private String latitude;
    private String longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        Intent detalle = getIntent();
        Bundle bundle = detalle.getBundleExtra("detalles");

        String name = bundle.getString("name");
        String description = bundle.getString("description");
        String photo = bundle.getString("photo");
        latitude = bundle.getString("latitude");
        longitude = bundle.getString("longitude");
        String address = bundle.getString("address");
        String phone = bundle.getString("phone");
        String email = bundle.getString("email");
        String instagram = bundle.getString("instagram");
        String facebook = bundle.getString("facebook");
        String twitter = bundle.getString("twitter");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            if (getSupportActionBar() !=null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setTitle(name);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        setupNavigationView();

        ImageView logo = (ImageView)findViewById(R.id.detalleLogo);
        TextView nombre = (TextView)findViewById(R.id.detalleNombre);
        TextView descripcion = (TextView)findViewById(R.id.detalleDescripcion);
        TextView direccion = (TextView)findViewById(R.id.detalleDireccion);
        Button telefono = (Button)findViewById(R.id.detalleTelefono);
        Button correo = (Button)findViewById(R.id.detalleEmail);
        Button Facebook = (Button)findViewById(R.id.detalleFacebook);
        Button Twitter = (Button)findViewById(R.id.detalleTwitter);
        Button Instagram = (Button)findViewById(R.id.detalleInstragram);

        Glide.with(logo.getContext()).load(photo).into(logo);
        nombre.setText(name);
        descripcion.setText(Html.fromHtml(description));
        direccion.setText(address);

        if (latitude!=null && longitude!=null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }else{
            (findViewById(R.id.mapLayout)).setVisibility(View.GONE);
        }
        if (phone!=null && !phone.isEmpty()) {
            telefono.setText(phone);
        }else
            telefono.setVisibility(View.GONE);

        if (email!=null && !email.isEmpty()) {
            correo.setText(email);
        }else
            correo.setVisibility(View.GONE);

        if (facebook!=null && !facebook.isEmpty()) {
            Facebook.setText(facebook);
        }else
            Facebook.setVisibility(View.GONE);

        if (twitter!=null && !twitter.isEmpty()) {
            Twitter.setText(twitter);
        }else
            Twitter.setVisibility(View.GONE);

        if (instagram!=null && !instagram.isEmpty()) {
            Instagram.setText(instagram);
        }else
            Instagram.setVisibility(View.GONE);



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detalle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupNavigationView() {
        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            ((TextView) findViewById(R.id.nombreUsuario)).setText(usuario.getNombre());
            ListView nav_list = (ListView) findViewById(R.id.nav_listView);
            ArrayList<NavItem> nav_items = new ArrayList<>();
            nav_items.add(new NavItem("Categorias", R.drawable.icon_category));
            nav_items.add(new NavItem("Premios", R.drawable.icon_trophy));
            nav_items.add(new NavItem("Perfil", R.drawable.icon_profile));
            nav_items.add(new NavItem("Cerrar Sesion", R.drawable.icon_logout));
            nav_list.setAdapter(new NavListAdapter(getApplicationContext(), nav_items));
            nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            startActivity(new Intent(DetalleActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 1:
                            startActivity(new Intent(DetalleActivity.this, PremioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 2:
                            startActivity(new Intent(DetalleActivity.this, Registro1Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 3:
                            PrefUtils.clearUsuario(getApplicationContext());
                            startActivity(new Intent(DetalleActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng ubicacion = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        googleMap.addMarker(new MarkerOptions().position(ubicacion).title("Marker in Sydney"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubicacion,15));
    }
}
