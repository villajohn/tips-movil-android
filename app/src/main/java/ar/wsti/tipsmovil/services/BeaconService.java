package ar.wsti.tipsmovil.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import ar.wsti.tipsmovil.AsyncHttpClientHandler;
import ar.wsti.tipsmovil.Constantes;
import ar.wsti.tipsmovil.MainActivity;
import ar.wsti.tipsmovil.fragments.DialogActivity;
import ar.wsti.tipsmovil.objetos.BeaconMessage;
import cz.msebera.android.httpclient.Header;

public class BeaconService extends Service implements BeaconConsumer {

    private BeaconManager beaconManager;
    private AudioManager audioManager;
    private SharedPreferences sharedPreferences;
    private SharedPreferences datosMarketing;
    private SharedPreferences.Editor editor;
    private Boolean isMarketingBusy = false;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.setBackgroundMode(true);
        //beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.ALTBEACON_LAYOUT));
        //konkakt?
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"));

        beaconManager.setBackgroundScanPeriod(3000l);
        beaconManager.setBackgroundBetweenScanPeriod(5000l);
        beaconManager.bind(this);

        datosMarketing = getSharedPreferences("marketing", 0);

        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
        SharedPreferences.Editor viewEdit = getView.edit();
        viewEdit.putBoolean("Actual_View", false);
        viewEdit.apply();
        Toast.makeText(BeaconService.this, "Servicio Beacon Creado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onBeaconServiceConnect() {
        try {
            Toast.makeText(BeaconService.this, "Servicio Beacon Conectado", Toast.LENGTH_SHORT).show();
            beaconManager.startRangingBeaconsInRegion(new Region("Beacon_iClas", Identifier.parse("F7826DA6-4FA2-4E98-8024-BC5B71E0893E"), null, null));

        } catch (RemoteException e) {
        }

        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    Toast.makeText(BeaconService.this, "Beacon Encontrado iCLAS está a: " + beacons.iterator().next().getDistance() + "metros", Toast.LENGTH_LONG).show();

                    Toast.makeText(BeaconService.this, "Sus datos: UUID: " + beacons.iterator().next().getId1() + "major: "+ beacons.iterator().next().getId2() + "minor:" + beacons.iterator().next().getId3(), Toast.LENGTH_SHORT).show();

                    getMarketing(beacons.iterator().next().getId1()+"", beacons.iterator().next().getId2()+"", beacons.iterator().next().getId3()+"");

                }
            }
        });

        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);

                Toast.makeText(BeaconService.this, "Beacon en una Region", Toast.LENGTH_SHORT).show();

                if (getView.getBoolean("Actual_View", false) == false) {

                    SharedPreferences usuario = getSharedPreferences("LogedUser", 0);
                    String idUsuario = usuario.getString("id", "0");
                    String language  = Locale.getDefault().getLanguage();

                    RequestParams requestParams = new RequestParams();
                    requestParams.put(Constantes.TAG_KEY, Constantes.key);
                    requestParams.put(Constantes.TAG_LANGUAGE, language);
                    requestParams.put("type", "beacon");
                    requestParams.put(Constantes.TAG_CUSTOMERS_ID, idUsuario);

                }


            }

            @Override
            public void didExitRegion(Region region) {
                Toast.makeText(BeaconService.this, "Beacon Saliendo de Region", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                Toast.makeText(BeaconService.this, "Becaon determinando Region", Toast.LENGTH_SHORT).show();
            }
        });


        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Toast.makeText(BeaconService.this, "Ha entrado a una region de Monitor Notifier", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void didExitRegion(Region region) {
                Toast.makeText(BeaconService.this, "Ha salido de una region de Monitor Notifier", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                try {
                    Identifier identifier = Identifier.parse("F7826DA6-4FA2-4E98-8024-BC5B71E0893E"); //beacon 1
                    beaconManager.startMonitoringBeaconsInRegion(new Region("iClas_Beacon", identifier, null, null));
                } catch (RemoteException e) {
                    Log.e("BeaconService", e.getMessage(), e);
                }
            }
        });

        try {
            Identifier identifier = Identifier.parse("F7826DA6-4FA2-4E98-8024-BC5B71E0893E"); //beacon 1
            beaconManager.startMonitoringBeaconsInRegion(new Region("iClas_Beacon", identifier, null, null));
        } catch (RemoteException e) {}
    }

    private void getMarketing(String UUID, String major, String minor) {
        isMarketingBusy = datosMarketing.getBoolean("isBusy", false);

        SharedPreferences usuario = getSharedPreferences("LogedUser", 0);
        String idUsuario = usuario.getString("id", "0");
        String language  = Locale.getDefault().getLanguage();

        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY, Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE, language);
        requestParams.put("uuid", UUID);
        requestParams.put("major", major);
        requestParams.put("minor", minor);
        requestParams.put(Constantes.TAG_CUSTOMERS_ID, idUsuario);
        requestParams.put("type", "beacon");
        AsyncHttpClientHandler.post("checkGPSZone", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                String message;
                JSONArray content;
                if (response != null) {
                    try {
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        message = Constantes.TAG_MESSAGE;
                        if (success) {
                            content = response.getJSONArray(Constantes.TAG_CONTENT);
                            for (int i = 0; i < content.length(); i++) {
                                JSONObject c = content.getJSONObject(i);
                                String idObject = c.getString(Constantes.TAG_ID);

                                //Crear el objeto
                                BeaconMessage beaconMessage = new BeaconMessage();
                                beaconMessage.setmId(idObject);
                                beaconMessage.setmName(c.getString(Constantes.TAG_NAME));
                                beaconMessage.setmPhoto(c.getString(Constantes.TAG_PHOTO));
                                beaconMessage.setmPublicationId(c.getString("type_publication_id"));
                                beaconMessage.setmDistance(c.getDouble("distance"));
                                beaconMessage.setmVideoUrl(c.getString("video_url"));
                                beaconMessage.setmExpoTime(c.getInt("expo_time"));
                                beaconMessage.setmTexto1(c.getString("texto1"));
                                beaconMessage.setmTexto2(c.getString("texto2"));
                                beaconMessage.setmLatitude(c.getDouble(Constantes.TAG_LATITUDE));
                                beaconMessage.setmLongitude(c.getDouble(Constantes.TAG_LONGITUDE));
                                beaconMessage.setmFar(c.getDouble("far"));
                                beaconMessage.setmWebsite(c.getString("website"));

                                SharedPreferences beac = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
                                SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
                                if (Integer.parseInt(beaconMessage.getmId()) != beac.getInt(idObject, 0) && getView.getBoolean("Actual_View", false) == false) {
                                    saveCheckingMessage(beaconMessage);
                                    break;
                                } else {
                                    if (getView.getBoolean("Actual_View", false) == false) {
                                        try {
                                            int timeToWait = 120;
                                            SharedPreferences beacSh = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
                                            String timeCreated = beacSh.getString("created", "2016-01-01 00:00:00");
                                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            Date date = formatter.parse(timeCreated);
                                            Date dt2 = new Date();

                                            long diff = dt2.getTime() - date.getTime();
                                            long elapsedTime = diff / (60 * 1000) % 60; //diff / 1000 % 60;
                                            long diffSeconds = diff / 1000 % 60;
                                            long secondsDiff = (elapsedTime * 60) + diffSeconds;
                                            if (secondsDiff > timeToWait && getView.getBoolean("Actual_View", false) == false) {
                                                saveCheckingMessage(beaconMessage);
                                            }
                                            break;
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        });

    }

    private void saveCheckingMessage(BeaconMessage beaconMessage) {
        SharedPreferences beac = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = beac.edit();
        editor.putInt(beaconMessage.getmId(), Integer.parseInt(beaconMessage.getmId()));
        editor.putString("created", getCurrentTimeStamp());
        editor.apply();

        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
        SharedPreferences.Editor editView = getView.edit();
        editView.putBoolean("Actual_View", true);
        editView.apply();

        showNotification("Nueva Oferta!", "Queremos mostrarte una nuevo mensjae que puede interesarte");
        showActivity(beaconMessage);

    }

    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

    public void showActivity(BeaconMessage beaconMessage) {
        Intent inte = new Intent();
        inte.setClass(this, DialogActivity.class);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        inte.putExtra("id", beaconMessage.getmId());
        inte.putExtra("name", beaconMessage.getmName());
        inte.putExtra("photo", beaconMessage.getmPhoto());
        inte.putExtra("pub_id", beaconMessage.getmPublicationId());
        inte.putExtra("distance", beaconMessage.getmDistance());
        inte.putExtra("video", beaconMessage.getmVideoUrl());
        inte.putExtra("expo_time", beaconMessage.getmExpoTime());
        inte.putExtra("texto1", beaconMessage.getmTexto1());
        inte.putExtra("texto2", beaconMessage.getmTexto2());
        inte.putExtra("latitude", beaconMessage.getmLatitude());
        inte.putExtra("longitude", beaconMessage.getmLongitude());
        inte.putExtra("far", beaconMessage.getmFar());
        inte.putExtra("website", beaconMessage.getmWebsite());
        startActivity(inte);
    }

    private static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

}
