package ar.wsti.tipsmovil.services;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.*;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ar.wsti.tipsmovil.AsyncHttpClientHandler;
import ar.wsti.tipsmovil.Constantes;
import ar.wsti.tipsmovil.MainActivity;
import ar.wsti.tipsmovil.R;
import ar.wsti.tipsmovil.activities.MarketingActivity;
import ar.wsti.tipsmovil.fragments.DialogActivity;
import ar.wsti.tipsmovil.objetos.BeaconMessage;
import cz.msebera.android.httpclient.Header;

public class BackgroundLocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private ArrayList<BeaconMessage> mMessagesList;
    private GoogleApiClient mLocationClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    // Flag that indicates if a request is underway.
    private boolean mInProgress;

    private BeaconMessage[] mMarketing;

    private Boolean servicesAvailable = false;

    private Boolean isMarketingBusy = false;

    SharedPreferences datosMarketing;
    SharedPreferences.Editor editor;

    public class LocalBinder extends Binder {
        public BackgroundLocationService getServerInstance() {
            return BackgroundLocationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInProgress = false;
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(Constantes.gLocationInterval);

        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(2000);

        servicesAvailable = servicesConnected();

        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mLocationClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates lSS = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        System.out.println("Entrada 1");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        System.out.println("Entrada 2");
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        System.out.println("Entrada 3");
                        break;
                }
            }
        });

        LocationManager lm = (LocationManager)this.getBaseContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        datosMarketing = getSharedPreferences("marketing", 0);

        editor = datosMarketing.edit();
        editor.putBoolean("isBusy", false);
        editor.commit();

        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
        SharedPreferences.Editor viewEdit = getView.edit();
        viewEdit.putBoolean("Actual_View", false);
        viewEdit.apply();

        /*getMarketing(11.6603, -70.1906);*/
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if(!servicesAvailable || mLocationClient.isConnected() || mInProgress)
            return START_STICKY;

        setUpLocationClientIfNeeded();
        if(!mLocationClient.isConnected() || !mLocationClient.isConnecting() && !mInProgress)
        {
            mInProgress = true;
            mLocationClient.connect();
        }

        return START_STICKY;
    }

    private void setUpLocationClientIfNeeded() {
        if(mLocationClient == null)
            mLocationClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocationRequest.setInterval(Constantes.gLocationInterval);
        getMarketing(location.getLatitude(), location.getLongitude());
    }

    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        // Turn off the request flag
        mInProgress = false;
        if(servicesAvailable && mLocationClient != null) {
            mLocationClient.disconnect();
            // Destroy the current location client
            mLocationClient = null;
        }
        // Display the connection status
        // Toast.makeText(this, DateFormat.getDateTimeInstance().format(new Date()) + ": Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Request location updates using static settings
        if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        }
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            // If no resolution is available, display an error dialog
        } else {
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Network lost. Please re-connect.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMarketing(Double latitude, Double longitude) {
        isMarketingBusy = datosMarketing.getBoolean("isBusy", false);

        SharedPreferences usuario = getSharedPreferences("LogedUser", 0);
        String idUsuario = usuario.getString("id", "0");
        String language  = Locale.getDefault().getLanguage();

        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY, Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE, language);
        requestParams.put(Constantes.TAG_LATITUDE, latitude);
        requestParams.put(Constantes.TAG_LONGITUDE, longitude);
        requestParams.put(Constantes.TAG_CUSTOMERS_ID, idUsuario);
        requestParams.put("type", "gps");
        AsyncHttpClientHandler.post("checkGPSZone", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                String message;
                JSONArray content;
                if (response != null) {
                    try {
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        message = Constantes.TAG_MESSAGE;
                        if (success) {
                            content = response.getJSONArray(Constantes.TAG_CONTENT);
                            for (int i = 0; i < content.length(); i++) {
                                JSONObject c = content.getJSONObject(i);
                                String idObject = c.getString(Constantes.TAG_ID);

                                //Crear el objeto
                                BeaconMessage beaconMessage = new BeaconMessage();
                                beaconMessage.setmId(idObject);
                                beaconMessage.setmName(c.getString(Constantes.TAG_NAME));
                                beaconMessage.setmPhoto(c.getString(Constantes.TAG_PHOTO));
                                beaconMessage.setmPublicationId(c.getString("type_publication_id"));
                                beaconMessage.setmDistance(c.getDouble("distance"));
                                beaconMessage.setmVideoUrl(c.getString("video_url"));
                                beaconMessage.setmExpoTime(c.getInt("expo_time"));
                                beaconMessage.setmTexto1(c.getString("texto1"));
                                beaconMessage.setmTexto2(c.getString("texto2"));
                                beaconMessage.setmLatitude(c.getDouble(Constantes.TAG_LATITUDE));
                                beaconMessage.setmLongitude(c.getDouble(Constantes.TAG_LONGITUDE));
                                beaconMessage.setmFar(c.getDouble("far"));
                                beaconMessage.setmWebsite(c.getString("website"));

                                SharedPreferences beac = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
                                SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
                                if (Integer.parseInt(beaconMessage.getmId()) != beac.getInt(idObject, 0) && getView.getBoolean("Actual_View", false) == false) {
                                    saveCheckingMessage(beaconMessage);
                                    break;
                                } else {
                                    if (getView.getBoolean("Actual_View", false) == false) {
                                        try {
                                            int timeToWait = 120;
                                            SharedPreferences beacSh = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
                                            String timeCreated = beacSh.getString("created", "2016-01-01 00:00:00");
                                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            Date date = formatter.parse(timeCreated);
                                            Date dt2 = new Date();

                                            long diff = dt2.getTime() - date.getTime();
                                            long elapsedTime = diff / (60 * 1000) % 60; //diff / 1000 % 60;
                                            long diffSeconds = diff / 1000 % 60;
                                            long secondsDiff = (elapsedTime * 60) + diffSeconds;
                                            if (secondsDiff > timeToWait && getView.getBoolean("Actual_View", false) == false) {
                                                saveCheckingMessage(beaconMessage);
                                            }
                                            break;
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        });

    }

    private void saveCheckingMessage(BeaconMessage beaconMessage) {
        SharedPreferences beac = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = beac.edit();
        editor.putInt(beaconMessage.getmId(), Integer.parseInt(beaconMessage.getmId()));
        editor.putString("created", getCurrentTimeStamp());
        editor.apply();

        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
        SharedPreferences.Editor editView = getView.edit();
        editView.putBoolean("Actual_View", true);
        editView.apply();

        showNotification("Nueva Oferta!", "Queremos mostrarte una nuevo mensjae que puede interesarte");
        showActivity(beaconMessage);

    }

    public void showActivity(BeaconMessage beaconMessage) {
        Intent inte = new Intent();
        inte.setClass(this, DialogActivity.class);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        inte.putExtra("id", beaconMessage.getmId());
        inte.putExtra("name", beaconMessage.getmName());
        inte.putExtra("photo", beaconMessage.getmPhoto());
        inte.putExtra("pub_id", beaconMessage.getmPublicationId());
        inte.putExtra("distance", beaconMessage.getmDistance());
        inte.putExtra("video", beaconMessage.getmVideoUrl());
        inte.putExtra("expo_time", beaconMessage.getmExpoTime());
        inte.putExtra("texto1", beaconMessage.getmTexto1());
        inte.putExtra("texto2", beaconMessage.getmTexto2());
        inte.putExtra("latitude", beaconMessage.getmLatitude());
        inte.putExtra("longitude", beaconMessage.getmLongitude());
        inte.putExtra("far", beaconMessage.getmFar());
        inte.putExtra("website", beaconMessage.getmWebsite());
        startActivity(inte);
    }

    private static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }


}
