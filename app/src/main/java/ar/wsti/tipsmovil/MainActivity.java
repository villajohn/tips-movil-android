package ar.wsti.tipsmovil;

import android.*;
import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.FirebaseApp;
import com.kontakt.sdk.android.ble.configuration.ActivityCheckConfiguration;
import com.kontakt.sdk.android.ble.configuration.ForceScanConfiguration;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.configuration.scan.ScanMode;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.device.BeaconRegion;
import com.kontakt.sdk.android.ble.exception.ScanError;
import com.kontakt.sdk.android.ble.filter.ibeacon.IBeaconFilter;
import com.kontakt.sdk.android.ble.filter.ibeacon.IBeaconFilters;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerContract;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.ScanStatusListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleScanStatusListener;
import com.kontakt.sdk.android.ble.rssi.RssiCalculator;
import com.kontakt.sdk.android.ble.rssi.RssiCalculators;
import com.kontakt.sdk.android.ble.spec.EddystoneFrameType;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import ar.wsti.tipsmovil.beacons.ApplicationComponent;
import ar.wsti.tipsmovil.beacons.DataManager;
import ar.wsti.tipsmovil.beacons.DialogBuilder;
import ar.wsti.tipsmovil.fragments.DialogActivity;
import ar.wsti.tipsmovil.objetos.BeaconMessage;
import ar.wsti.tipsmovil.objetos.Categoria;
import ar.wsti.tipsmovil.objetos.NavItem;
import ar.wsti.tipsmovil.objetos.Usuario;
import ar.wsti.tipsmovil.services.BackgroundLocationService;
import ar.wsti.tipsmovil.services.BeaconService;
import ar.wsti.tipsmovil.utils.GooglePlayServicesHelper;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private GridView categoriasGrid;
    private CategoriasAdapter adapter;
    private ArrayList<Categoria> categoriaArrayList;
    private CategoriasAdapter.OnCategoriaSeleccionadoListener listener;

    private ProgressDialog pDialog;
    private View mMainFormView;
    private DrawerLayout drawer;
    private String mActivityTitle;
    private ActionBarDrawerToggle mDrawerToggle;

    private View mLayout;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE = 4;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    private GoogleApiClient mLocationClient;
    public static Location mLastLocation;
    private boolean mFirstTimeLoadingActivity;
    private LocationRequest mLocationRequest;
    private Boolean servicesAvailable = false;
    private LocationSettingsRequest mLocationSettingsRequest;

    private DataManager mDataManager;
    ApplicationComponent applicationComponent;

    //KONTAKT BEACONS
    private ProximityManagerContract proximityManager;
    private Boolean isMarketingBusy = false;
    SharedPreferences datosMarketing;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (proximityManager == null) {
            startKontakt();
        }


        //*** LOCATION
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(Constantes.gLocationInterval);

        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(2000);

        servicesAvailable = servicesConnected();

        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mLocationClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates lSS = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        verifyPermission();
                        System.out.println("Entrada 1");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        System.out.println("Entrada 2");
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        System.out.println("Entrada 3");
                        break;
                }
            }
        });

        //*** END LOCATION



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            if (getSupportActionBar() !=null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_main);
        mActivityTitle = getTitle().toString();
        setupDrawer();
        setupNavigationView();

        categoriasGrid = (GridView)findViewById(R.id.gridView);
        categoriaArrayList = new ArrayList<>();
        mMainFormView = findViewById(R.id.MainForm);
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Cargando contenido...");
        pDialog.setCancelable(false);

        listener = new CategoriasAdapter.OnCategoriaSeleccionadoListener() {
            @Override
            public void OnCategoriaSeleccionado(String id,String nombre) {
                Intent intent = new Intent(MainActivity.this,NegociosActivity.class);
                intent.putExtra("id",id);
                intent.putExtra("nombre",nombre);
                startActivity(intent);
            }
        };

        boolean checkSer = checkPlayServices();
        if(checkSer) {
            verifyPermission();
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
            Intent locationService = new Intent( this, BackgroundLocationService.class );
            startService(locationService);
        }


        try{
            obtenerCategorias();
        }catch(JSONException e){
            e.printStackTrace();
        }

        datosMarketing = getSharedPreferences("marketing", 0);

        editor = datosMarketing.edit();
        editor.putBoolean("isBusy", false);
        editor.commit();

        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
        SharedPreferences.Editor viewEdit = getView.edit();
        viewEdit.putBoolean("Actual_View", false);
        viewEdit.apply();

    }

    private void configureProximityManager() {
        proximityManager.configuration()
                .scanMode(ScanMode.BALANCED)
                .scanPeriod(ScanPeriod.create(TimeUnit.SECONDS.toMillis(10), TimeUnit.SECONDS.toMillis(20)))
                .activityCheckConfiguration(ActivityCheckConfiguration.DEFAULT);
    }

    private void configureListeners() {
        proximityManager.setIBeaconListener(createIBeaconListener());
        proximityManager.setScanStatusListener(createScanStatusListener());
    }

    private void configureSpaces() {
        IBeaconRegion region = new BeaconRegion.Builder()
                .identifier("Tips Beacons")
                .proximity(UUID.fromString("F7826DA6-4FA2-4E98-8024-BC5B71E0893E"))
                .major(BeaconRegion.ANY_MAJOR)
                .minor(BeaconRegion.ANY_MINOR)
                .build();

        proximityManager.spaces().iBeaconRegion(region);
    }

    private void configureFilters() {
        proximityManager.filters().iBeaconFilter(IBeaconFilters.newProximityUUIDFilter(UUID.fromString("F7826DA6-4FA2-4E98-8024-BC5B71E0893E")));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (proximityManager == null) {
            startKontakt();
        }
        int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            checkPermissionAndStart();
        } else {
            startScanning();
        }
    }

    @Override
    protected void onStop() {
        proximityManager.stopScanning();
        super.onStop();
    }

    private void startScanning() {
        proximityManager.configuration()
                .scanMode(ScanMode.BALANCED)
                .scanPeriod(ScanPeriod.RANGING)
                .activityCheckConfiguration(ActivityCheckConfiguration.DISABLED)
                .forceScanConfiguration(ForceScanConfiguration.DISABLED)
                .deviceUpdateCallbackInterval(TimeUnit.SECONDS.toMillis(5))
                .rssiCalculator(RssiCalculators.DEFAULT)
                .cacheFileName("ExampleKontakt")
                .resolveShuffledInterval(3)
                .monitoringEnabled(true)
                .monitoringSyncInterval(10)
                .eddystoneFrameTypes(Arrays.asList(EddystoneFrameType.UID, EddystoneFrameType.URL));

        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private IBeaconListener createIBeaconListener() {
        return new SimpleIBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice ibeacon, IBeaconRegion region) {
                //Toast.makeText(MainActivity.this, "IBeacon discovered! " + ibeacon.getProximityUUID().toString() + " MAjor: " + ibeacon.getMajor() + " Minor: " + ibeacon.getMinor() + "", Toast.LENGTH_SHORT).show();
                getMarketing(ibeacon.getProximityUUID().toString(), ibeacon.getMajor(), ibeacon.getMinor());
            }
        };
    }

    private ScanStatusListener createScanStatusListener() {
        return new SimpleScanStatusListener() {
            @Override
            public void onScanStart() {

                //showToast("Scanning started");
            }

            @Override
            public void onScanStop() {
                //showToast("Scanning stopped");
            }

            @Override
            public void onScanError(ScanError exception) {
                showToast(exception.getMessage());
            }
        };
    }

    @Override
    protected void onDestroy() {
        proximityManager.disconnect();
        proximityManager = null;
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (categoriasGrid.getAdapter() == null){
            try{
                obtenerCategorias();
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
    }

    private void setupNavigationView(){
        Usuario usuario = PrefUtils.getUsuario(getApplicationContext());
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            ((TextView) findViewById(R.id.nombreUsuario)).setText(usuario.getNombre());
            ListView nav_list = (ListView) findViewById(R.id.nav_listView);
            ArrayList<NavItem> nav_items = new ArrayList<>();
            nav_items.add(new NavItem("Categorias", R.drawable.icon_category));
            nav_items.add(new NavItem("Premios", R.drawable.icon_trophy));
            nav_items.add(new NavItem("Perfil", R.drawable.icon_profile));
            nav_items.add(new NavItem("Intereses", R.drawable.icon_favorites_purple));
            nav_items.add(new NavItem("Cerrar Sesion", R.drawable.icon_logout));
            nav_list.setAdapter(new NavListAdapter(getApplicationContext(), nav_items));
            nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 1:
                            startActivity(new Intent(MainActivity.this, PremioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 2:
                            startActivity(new Intent(MainActivity.this,Registro1Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 3:
                            startActivity(new Intent(MainActivity.this, Registro2Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                        case 4:
                            PrefUtils.clearUsuario(getApplicationContext());
                            startActivity(new Intent(MainActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            break;
                    }
                }
            });
        }
    }

    public void obtenerCategorias() throws JSONException {
        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY,Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE,"es");
        AsyncHttpClientHandler.get("getFidelidadCategories", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                JSONArray content;
                if (response != null) {
                    try {
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        if (success) {
                            content = response.getJSONArray(Constantes.TAG_CONTENT);
                            for (int i = 0;i<content.length();i++) {
                                JSONObject c = content.getJSONObject(i);


                                String id = c.getString(Constantes.TAG_ID);
                                String nombre = c.getString(Constantes.TAG_NAME);
                                String foto = c.getString(Constantes.TAG_PHOTO);
                                Categoria categoria = new Categoria(id,nombre,foto);
                                categoriaArrayList.add(categoria);
                            }
                            showProgress(false);
                            adapter = new CategoriasAdapter(MainActivity.this,categoriaArrayList,listener);
                            categoriasGrid.setAdapter(adapter);

                        } else {
                            showProgress(false);
                        }
                    } catch (JSONException e) {
                        showProgress(false);
                        e.printStackTrace();
                    }
                } else {
                    showProgress(false);
                }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject responseError) {
                    showProgress(false);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                    alertDialog.setTitle("Error de conexion");
                    alertDialog.setMessage("Hubo un error tratanto de realizar la solicitud");
                    alertDialog.setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                obtenerCategorias();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog.show();
                }

                @Override
                public void onCancel(){
                    finish();
                }

            });

    }
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return (mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item));
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle("Main Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getSupportActionBar() !=null)
                    getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(mDrawerToggle);
    }


    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mMainFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mMainFormView.animate().setDuration(shortAnimTime).alpha( show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMainFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        if (show)
            pDialog.show();
        else
            pDialog.dismiss();

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("Message", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mFirstTimeLoadingActivity) {
            mFirstTimeLoadingActivity = false;
            //setupFragment();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        verifyPermission();
    }

    private void verifyPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            evaluateAccessCoarseLocationPermission();
        } else {
            determinateGeolocation();
        }
    }

    public void evaluateAccessCoarseLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestAccessCoarseLocationPermissions();
        } else {
            determinateGeolocation();
        }
    }

    private void requestAccessCoarseLocationPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(mLayout, R.string.permissions_access_coarse_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // http://stackoverflow.com/questions/35989288/onrequestpermissionsresult-not-being-called-in-fragment-if-defined-in-both-fragm
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE);
        }
    }

    private void determinateGeolocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            //LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        }
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Network lost. Please re-connect.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            // If no resolution is available, display an error dialog
        } else {
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult result) {
        final Status status = result.getStatus();
        final LocationSettingsStates lSS = result.getLocationSettingsStates();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can
                // initialize location requests here.
                verifyPermission();
                System.out.println("Entrada 1");
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                System.out.println("Entrada 2");
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way
                // to fix the settings so we won't show the dialog.
                System.out.println("Entrada 3");
                break;
        }
    }

    private void startKontakt() {
        KontaktSDK.initialize("BgJTThKjKWmqSDeIJebUoelzIjhYMPEE");
        proximityManager = new ProximityManager(this);
        configureProximityManager();
        configureListeners();
        configureSpaces();
        configureFilters();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (100 == requestCode) {
                //same request code as was in request permission
                startScanning();
            }

        } else {
           showToast("Si no otorgas el permiso de localización de tu dispositivo no podremos ofrecerte ofertas que puedan interesarte");
        }
    }

    private void checkPermissionAndStart() {
        int checkSelfPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (PackageManager.PERMISSION_GRANTED == checkSelfPermissionResult) {
            //already granted
            startScanning();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                //we should show some explanation for user here
                showToast("Es importante garantizar los permisos de localización de tu positivo para brindarte una mejor experiencia");
            } else {
                //request permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
            }
        }
    }

    private void getMarketing(String uuid, int major, int minor) {
        isMarketingBusy = datosMarketing.getBoolean("isBusy", false);

        SharedPreferences usuario = getSharedPreferences("LogedUser", 0);
        String idUsuario = usuario.getString("id", "0");
        String language  = Locale.getDefault().getLanguage();

        RequestParams requestParams = new RequestParams();
        requestParams.put(Constantes.TAG_KEY, Constantes.key);
        requestParams.put(Constantes.TAG_LANGUAGE, language);
        requestParams.put(Constantes.TAG_UUID, uuid);
        requestParams.put(Constantes.TAG_MAJOR, major);
        requestParams.put(Constantes.TAG_MINOR, minor);
        requestParams.put(Constantes.TAG_CUSTOMERS_ID, idUsuario);
        requestParams.put("type", "beacon");
        AsyncHttpClientHandler.post("checkGPSZone", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                boolean success;
                String message;
                JSONArray content;
                if (response != null) {
                    try {
                        success = response.getBoolean(Constantes.TAG_SUCCESS);
                        message = Constantes.TAG_MESSAGE;
                        if (success) {
                            content = response.getJSONArray(Constantes.TAG_CONTENT);
                            for (int i = 0; i < content.length(); i++) {
                                JSONObject c = content.getJSONObject(i);
                                String idObject = c.getString(Constantes.TAG_ID);

                                //Crear el objeto
                                BeaconMessage beaconMessage = new BeaconMessage();
                                beaconMessage.setmId(idObject);
                                beaconMessage.setmName(c.getString(Constantes.TAG_NAME));
                                beaconMessage.setmPhoto(c.getString(Constantes.TAG_PHOTO));
                                beaconMessage.setmPublicationId(c.getString("type_publication_id"));
                                beaconMessage.setmDistance(c.getDouble("distance"));
                                beaconMessage.setmVideoUrl(c.getString("video_url"));
                                beaconMessage.setmExpoTime(c.getInt("expo_time"));
                                beaconMessage.setmTexto1(c.getString("texto1"));
                                beaconMessage.setmTexto2(c.getString("texto2"));
                                beaconMessage.setmLatitude((!c.getString(Constantes.TAG_LATITUDE).isEmpty() ? c.getDouble(Constantes.TAG_LATITUDE) : 0));
                                beaconMessage.setmLongitude((!c.getString(Constantes.TAG_LONGITUDE).isEmpty() ? c.getDouble(Constantes.TAG_LONGITUDE) : 0));
                                beaconMessage.setmFar(c.getDouble("far"));
                                beaconMessage.setmWebsite(c.getString("website"));
                                beaconMessage.setmDescription(c.getString("description"));

                                Toast.makeText(MainActivity.this, c.getString("texto1")+"", Toast.LENGTH_SHORT).show();

                                SharedPreferences beac = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
                                SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);

                                Toast.makeText(MainActivity.this, "Estado: " + getView.getBoolean("Actual_View", false), Toast.LENGTH_SHORT).show();
                                if (Integer.parseInt(beaconMessage.getmId()) != beac.getInt(idObject, 0) && getView.getBoolean("Actual_View", false) == false) {
                                    saveCheckingMessage(beaconMessage);
                                    break;
                                } else {
                                    if (getView.getBoolean("Actual_View", false) == false) {
                                        try {
                                            int timeToWait = 120;
                                            SharedPreferences beacSh = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
                                            String timeCreated = beacSh.getString("created", "2016-01-01 00:00:00");
                                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            Date date = formatter.parse(timeCreated);
                                            Date dt2 = new Date();

                                            long diff = dt2.getTime() - date.getTime();
                                            long elapsedTime = diff / (60 * 1000) % 60; //diff / 1000 % 60;
                                            long diffSeconds = diff / 1000 % 60;
                                            long secondsDiff = (elapsedTime * 60) + diffSeconds;
                                            if (secondsDiff > timeToWait && getView.getBoolean("Actual_View", false) == false) {
                                                saveCheckingMessage(beaconMessage);
                                                break;
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void saveCheckingMessage(BeaconMessage beaconMessage) {
        SharedPreferences beac = getSharedPreferences(beaconMessage.getmId(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = beac.edit();
        editor.putInt(beaconMessage.getmId(), Integer.parseInt(beaconMessage.getmId()));
        editor.putString("created", getCurrentTimeStamp());
        editor.apply();

        SharedPreferences getView = getSharedPreferences("Beacon_View", Context.MODE_PRIVATE);
        SharedPreferences.Editor editView = getView.edit();
        editView.putBoolean("Actual_View", true);
        editView.apply();

        showNotification("Nueva Oferta!", "Queremos mostrarte una nuevo mensjae que puede interesarte");
        showActivity(beaconMessage);
    }

    public void showActivity(BeaconMessage beaconMessage) {
        Intent inte = new Intent();
        inte.setClass(this, DialogActivity.class);
        inte.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        inte.putExtra("id", beaconMessage.getmId());
        inte.putExtra("name", beaconMessage.getmName());
        inte.putExtra("photo", beaconMessage.getmPhoto());
        inte.putExtra("pub_id", beaconMessage.getmPublicationId());
        inte.putExtra("distance", beaconMessage.getmDistance());
        inte.putExtra("video", beaconMessage.getmVideoUrl());
        inte.putExtra("expo_time", beaconMessage.getmExpoTime());
        inte.putExtra("texto1", beaconMessage.getmTexto1());
        inte.putExtra("texto2", beaconMessage.getmTexto2());
        inte.putExtra("latitude", beaconMessage.getmLatitude());
        inte.putExtra("longitude", beaconMessage.getmLongitude());
        inte.putExtra("far", beaconMessage.getmFar());
        inte.putExtra("website", beaconMessage.getmWebsite());
        inte.putExtra("description", beaconMessage.getmDescription());
        startActivity(inte);
    }

    private static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

}
