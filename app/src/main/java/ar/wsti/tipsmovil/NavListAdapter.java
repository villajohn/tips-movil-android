package ar.wsti.tipsmovil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ar.wsti.tipsmovil.objetos.NavItem;

public class NavListAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<NavItem> items;
    public NavListAdapter(Context c, ArrayList<NavItem> items) {
        mContext = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public NavItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.nav_listitem, viewGroup, false);
        }

        NavItem item = getItem(position);

        TextView nombre = (TextView) view.findViewById(R.id.itemName);
        ImageView imagen = (ImageView) view.findViewById(R.id.itemImage);

        nombre.setText(item.getNombre());
        Glide.with(imagen.getContext()).load(item.getImagen()).into(imagen);


        return view;
    }

}
