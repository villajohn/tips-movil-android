package ar.wsti.tipsmovil;

import com.loopj.android.http.*;

public class AsyncHttpClientHandler {
    private static final String BASE_URL = "http://mipsicomama.com/wsti_multi/Api/";
//    private static final String BASE_URL = "http://192.168.0.101/multi_wsti/Api/";
//    private static final String BASE_URL = "http://192.168.1.106/multi_wsti/Api/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
